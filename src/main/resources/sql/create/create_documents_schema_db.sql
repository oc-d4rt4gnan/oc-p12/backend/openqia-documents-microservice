CREATE SCHEMA IF NOT EXISTS pgdocuments AUTHORIZATION postgres;

SET search_path TO extensions, pgdocuments;

CREATE TABLE IF NOT EXISTS pgdocuments.companies
(
    id uuid DEFAULT uuid_generate_v4(),
    logo_svg VARCHAR NOT NULL,
    name VARCHAR NOT NULL,
    address VARCHAR NOT NULL,
    description VARCHAR NOT NULL,
    phone_number VARCHAR NOT NULL,
    mail VARCHAR NOT NULL,
    bank_details VARCHAR(200) NOT NULL,
    information VARCHAR NOT NULL,
    PRIMARY KEY (id)
);

COMMENT ON TABLE pgdocuments.companies IS 'Contains company informations for a document.';
COMMENT ON COLUMN pgdocuments.companies.id IS 'The table''s arbitrary primary key.';
COMMENT ON COLUMN pgdocuments.companies.logo_svg IS 'The company logo at the time of editing.';
COMMENT ON COLUMN pgdocuments.companies.name IS 'The company name at the time of editing.';
COMMENT ON COLUMN pgdocuments.companies.address IS 'The company address at the time of editing.';
COMMENT ON COLUMN pgdocuments.companies.description IS 'The company description at the time of editing.';
COMMENT ON COLUMN pgdocuments.companies.phone_number IS 'The company phone number at the time of editing.';
COMMENT ON COLUMN pgdocuments.companies.mail IS 'The company mail at the time of editing.';
COMMENT ON COLUMN pgdocuments.companies.bank_details IS 'The company bank details at the time of editing.';
COMMENT ON COLUMN pgdocuments.companies.information IS 'Other company informations like SIRET at the time of editing.';

CREATE TABLE IF NOT EXISTS pgdocuments.clients
(
    id uuid DEFAULT uuid_generate_v4(),
    name VARCHAR NOT NULL,
    address VARCHAR NOT NULL,
    PRIMARY KEY (id)
);

COMMENT ON TABLE pgdocuments.clients IS 'Contains client informations for a document.';
COMMENT ON COLUMN pgdocuments.clients.id IS 'The table''s arbitrary primary key.';
COMMENT ON COLUMN pgdocuments.clients.name IS 'The client fullname at the time of editing.';
COMMENT ON COLUMN pgdocuments.clients.address IS 'The client address at the time of editing.';

CREATE TABLE IF NOT EXISTS pgdocuments.documents_informations
(
    id uuid DEFAULT uuid_generate_v4(),
    validity_message VARCHAR NOT NULL,
    payment_method VARCHAR NOT NULL,
    conditions VARCHAR NOT NULL,
    PRIMARY KEY (id)
);

COMMENT ON TABLE pgdocuments.documents_informations IS 'Contains additionnal informations for a document.';
COMMENT ON COLUMN pgdocuments.documents_informations.id IS 'The table''s arbitrary primary key.';
COMMENT ON COLUMN pgdocuments.documents_informations.validity_message IS 'The message indicating the validity period of the document at the time of editing.';
COMMENT ON COLUMN pgdocuments.documents_informations.payment_method IS 'The message indicating the different methods of payment accepted at the time the document is edited.';
COMMENT ON COLUMN pgdocuments.documents_informations.conditions IS 'The message indicating the legal requirements at the time the document is edited.';

CREATE TABLE IF NOT EXISTS pgdocuments.documents
(
    reference VARCHAR NOT NULL,
    author VARCHAR NOT NULL,
    document_type VARCHAR(10) NOT NULL,
    creation_date TIMESTAMP NOT NULL,
    creation_location VARCHAR NOT NULL,
    update_date TIMESTAMP NOT NULL,
    client_id uuid NOT NULL,
    company_id uuid NOT NULL,
    subject VARCHAR NOT NULL,
    document_informations_id uuid NOT NULL,
    total_amount_excluding_tax DOUBLE PRECISION NOT NULL,
    total_vat DOUBLE PRECISION NOT NULL,
    total_amount_including_tax DOUBLE PRECISION NOT NULL,
    discount_excluding_tax DOUBLE PRECISION,
    net_total_excluding_tax DOUBLE PRECISION,
    discount_including_tax DOUBLE PRECISION,
    status varchar(10) NOT NULL,
    PRIMARY KEY (reference, document_type),
    FOREIGN KEY (client_id) REFERENCES pgdocuments.clients(id),
    FOREIGN KEY (company_id) REFERENCES pgdocuments.companies(id),
    FOREIGN KEY (document_informations_id) REFERENCES pgdocuments.documents_informations(id)
);

COMMENT ON TABLE pgdocuments.documents IS 'Contains the main information about a document.';
COMMENT ON COLUMN pgdocuments.documents.reference IS 'The document''s accounting reference.';
COMMENT ON COLUMN pgdocuments.documents.author IS 'The author of the document.';
COMMENT ON COLUMN pgdocuments.documents.document_type IS 'Type of document (e.g. quotation or invoice)';
COMMENT ON COLUMN pgdocuments.documents.creation_date IS 'Document creation date.';
COMMENT ON COLUMN pgdocuments.documents.creation_location IS 'The document''s place of creation appearing on the document.';
COMMENT ON COLUMN pgdocuments.documents.update_date IS 'The date on which the document was last updated.';
COMMENT ON COLUMN pgdocuments.documents.client_id IS 'The foreign key linking to the clients table.';
COMMENT ON COLUMN pgdocuments.documents.company_id IS 'The foreign key linking to the companies table.';
COMMENT ON COLUMN pgdocuments.documents.subject IS 'The title/object/subject of the document.';
COMMENT ON COLUMN pgdocuments.documents.document_informations_id IS 'The foreign key linking to the documents_informations table.';
COMMENT ON COLUMN pgdocuments.documents.total_amount_excluding_tax IS 'The total amount excluding taxes of the document lines.';
COMMENT ON COLUMN pgdocuments.documents.total_vat IS 'The total VAT amount of the document lines.';
COMMENT ON COLUMN pgdocuments.documents.total_amount_including_tax IS 'The total amount including taxes of the document lines.';
COMMENT ON COLUMN pgdocuments.documents.discount_excluding_tax IS 'The amount of the document''s discount before tax.';
COMMENT ON COLUMN pgdocuments.documents.net_total_excluding_tax IS 'The total with the discount before taxes.';
COMMENT ON COLUMN pgdocuments.documents.discount_including_tax IS 'The amount of the document''s discount after tax.';
COMMENT ON COLUMN pgdocuments.documents.status IS 'Document status (e.g. pending, accepted, rejected, paid).';

CREATE TABLE IF NOT EXISTS pgdocuments.documents_lines
(
    id SERIAL PRIMARY KEY,
    documents_ref VARCHAR NOT NULL,
    documents_type VARCHAR NOT NULL,
    section VARCHAR,
    article_label VARCHAR NOT NULL,
    quantity INT NOT NULL,
    unit_price DOUBLE PRECISION NOT NULL,
    vat DOUBLE PRECISION NOT NULL,
    total_excluding_taxes DOUBLE PRECISION NOT NULL,
    total DOUBLE PRECISION NOT NULL,
    FOREIGN KEY (documents_ref,documents_type) REFERENCES pgdocuments.documents(reference,document_type)
);

COMMENT ON TABLE pgdocuments.documents_lines IS 'Contains all informations for each line of a document.';
COMMENT ON COLUMN pgdocuments.documents_lines.id IS 'The table''s arbitrary primary key.';
COMMENT ON COLUMN pgdocuments.documents_lines.documents_ref IS 'The foreign key linking to the documents table.';
COMMENT ON COLUMN pgdocuments.documents_lines.documents_type IS 'The foreign key linking to the documents table.';
COMMENT ON COLUMN pgdocuments.documents_lines.section IS 'The name of the section containing the document line.';
COMMENT ON COLUMN pgdocuments.documents_lines.article_label IS 'The label of the article in the document line.';
COMMENT ON COLUMN pgdocuments.documents_lines.quantity IS 'The quantity of the article in the document line.';
COMMENT ON COLUMN pgdocuments.documents_lines.unit_price IS 'The unit price of the article in the document line.';
COMMENT ON COLUMN pgdocuments.documents_lines.vat IS 'The VAT of the article in the document line.';
COMMENT ON COLUMN pgdocuments.documents_lines.total_excluding_taxes IS 'The total of the document line without VAT.';
COMMENT ON COLUMN pgdocuments.documents_lines.total IS 'The total of the document line.';