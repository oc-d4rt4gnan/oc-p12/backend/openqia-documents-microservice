package fr.d4rt4gnan.openqia.documents;


import fr.d4rt4gnan.openqia.documents.api.DocumentsApiController;
import org.springdoc.core.SpringDocUtils;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DocumentsMicroserviceApplication {

	public static void main(String[] args) {
		SpringDocUtils.getConfig().addRestControllers(DocumentsApiController.class);
		SpringApplication.run(DocumentsMicroserviceApplication.class, args);
	}

}
