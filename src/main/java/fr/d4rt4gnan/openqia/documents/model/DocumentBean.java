package fr.d4rt4gnan.openqia.documents.model;


import fr.d4rt4gnan.openqia.documents.technical.converters.StatusConverter;
import fr.d4rt4gnan.openqia.documents.technical.converters.TypeConverter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.util.Objects;


/**
 * @author D4RT4GNaN
 * @since 14/03/2022
 */
@Entity
@Table(name = "documents", schema = "pgdocuments")
public class DocumentBean {
    
    private String    reference;
    private String    author;
    // Client
    private String    clientName;
    private String    clientAddress;
    // Company
    private String    companyLogo;
    private String    companyName;
    private String    companyAddress;
    private String    companyDescription;
    private String    companyTelephone;
    private String    companyEmail;
    private String    companyBankDetails;
    private String    companyInformation;
    //
    @Convert(converter = StatusConverter.class)
    private Status    status;
    @Convert(converter = TypeConverter.class)
    private Type      type;
    private Timestamp creationDate;
    private String    creationLocation;
    private Timestamp updateDate;
    private String    subject;
    private String    content;
    private String    validity;
    private String    paymentMethods;
    private String    conditions;
    private Double    totalAmountExcludingTax;
    private Double    totalVAT;
    private Double    totalAmountIncludingTax;
    private Double    discountExcludingTax;
    private Double    netTotalExcludingTax;
    private Double    discountIncludingTax;
    
    /**
     * Basic DocumentBean constructor
     */
    public DocumentBean () {
        long now = System.currentTimeMillis();
    
        reference               = "";
        author                  = "";
        clientName              = "";
        clientAddress           = "";
        companyLogo             = "";
        companyName             = "";
        companyAddress          = "";
        companyDescription      = "";
        companyTelephone        = "";
        companyEmail            = "";
        companyBankDetails      = "";
        companyInformation      = "";
        status                  = Status.PENDING;
        type                    = Type.UNDEFINED;
        creationDate            = new Timestamp(now);
        creationLocation        = "";
        updateDate              = new Timestamp(now);
        subject                 = "";
        content                 = "";
        validity                = "";
        paymentMethods          = "";
        conditions              = "";
        totalAmountExcludingTax = 0.0;
        totalVAT                = 0.2;
        totalAmountIncludingTax = 0.0;
        discountExcludingTax    = null;
        netTotalExcludingTax    = null;
        discountIncludingTax    = null;
    }
    
    /**
     * Getter of the Reference attribute.
     *
     * @return the reference of the document in String format.
     */
    @Id
    @Column(name = "reference")
    public String getReference () {
        return reference;
    }
    
    /**
     * Getter of the Author attribute.
     *
     * @return the author of the document in String format.
     */
    @Column(name = "author")
    public String getAuthor () {
        return author;
    }
    
    /**
     * Getter of the ClientName attribute.
     *
     * @return the name of the client in String format.
     */
    @NotNull
    @Column(name = "client_name")
    public String getClientName () {
        return clientName;
    }
    
    /**
     * Getter of the ClientAddress attribute.
     *
     * @return the address of the client in String format.
     */
    @NotNull
    @Column(name = "client_address")
    public String getClientAddress () {
        return clientAddress;
    }
    
    /**
     * Getter of the CompanyLogo attribute.
     *
     * @return the logo of the company in String format.
     */
    @NotNull
    @Column(name = "company_logo_svg")
    public String getCompanyLogo () {
        return companyLogo;
    }
    
    /**
     * Getter of the CompanyName attribute.
     *
     * @return the name of the company in String format.
     */
    @NotNull
    @Column(name = "company_name")
    public String getCompanyName () {
        return companyName;
    }
    
    /**
     * Getter of the CompanyAddress attribute.
     *
     * @return the address of the company in String format.
     */
    @NotNull
    @Column(name = "company_address")
    public String getCompanyAddress () {
        return companyAddress;
    }
    
    /**
     * Getter of the CompanyDescription attribute.
     *
     * @return the company description in String format.
     */
    @Column(name = "company_description")
    public String getCompanyDescription () {
        return companyDescription;
    }
    
    /**
     * Getter of the CompanyTelephone attribute.
     *
     * @return the phone number of the company in String format.
     */
    @NotNull
    @Column(name = "company_telephone")
    public String getCompanyTelephone () {
        return companyTelephone;
    }
    
    /**
     * Getter of the CompanyEmail attribute.
     *
     * @return the email of the company in String format.
     */
    @NotNull
    @Column(name = "company_email")
    public String getCompanyEmail () {
        return companyEmail;
    }
    
    /**
     * Getter of the CompanyBankDetails attribute.
     *
     * @return the bank details information of the company in String format.
     */
    @NotNull
    @Column(name = "company_bank_details", length = 175)
    public String getCompanyBankDetails () {
        return companyBankDetails;
    }
    
    /**
     * Getter of the CompanyInformation attribute.
     *
     * @return the information  of the company in String format.
     */
    @NotNull
    @Column(name = "company_information")
    public String getCompanyInformation () {
        return companyInformation;
    }
    
    /**
     * Getter of the Status attribute.
     *
     * @return the status of the document in String format.
     */
    @NotNull
    @Column(name = "status", length = 10)
    public Status getStatus () {
        return status;
    }
    
    /**
     * Getter of the Type attribute.
     *
     * @return the type of the document in String format.
     */
    @NotNull
    @Column(name = "document_type", length = 10)
    public Type getType () {
        return type;
    }
    
    /**
     * Getter of the CreationDate attribute.
     *
     * @return the date of creation of the document in Timestamp format.
     */
    @NotNull
    @Column(name = "creation_date")
    public Timestamp getCreationDate () {
        return creationDate;
    }
    
    /**
     * Getter of the CreationLocation attribute.
     *
     * @return the location of creation of the document in String format.
     */
    @NotNull
    @Column(name = "creation_location")
    public String getCreationLocation () {
        return creationLocation;
    }
    
    /**
     * Getter of the UpdateDate attribute.
     *
     * @return the date of the update of the document in Timestamp format.
     */
    @NotNull
    @Column(name = "update_date")
    public Timestamp getUpdateDate () {
        return updateDate;
    }
    
    /**
     * Getter of the Subject attribute.
     *
     * @return the subject of the document in String format.
     */
    @NotNull
    @Column(name = "subject")
    public String getSubject () {
        return subject;
    }
    
    /**
     * Getter of the Content attribute.
     *
     * @return the content of the document in String format (JSON).
     */
    @Column(name = "document_content")
    public String getContent () {
        return content;
    }
    
    /**
     * Getter of the ValidityMessage attribute.
     *
     * @return the validity message in the document in String format.
     */
    @Column(name = "validity_message")
    public String getValidity () {
        return validity;
    }
    
    /**
     * Getter of the PaymentMethods attribute.
     *
     * @return the payment methods information in the document in String format.
     */
    @Column(name = "payment_method")
    public String getPaymentMethods () {
        return paymentMethods;
    }
    
    /**
     * Getter of the Conditions attribute.
     *
     * @return the conditions of the document in String format.
     */
    @Column(name = "conditions")
    public String getConditions () {
        return conditions;
    }
    
    /**
     * Getter of the TotalAmountExcludingTax attribute.
     *
     * @return the total amount excluding tax of the document in Double format.
     */
    @Column(name = "total_amount_excluding_tax")
    public Double getTotalAmountExcludingTax () {
        return totalAmountExcludingTax;
    }
    
    /**
     * Getter of the TotalVAT attribute.
     *
     * @return the total value added tax of the document in Double format.
     */
    @Column(name = "total_vat")
    public Double getTotalVAT () {
        return totalVAT;
    }
    
    /**
     * Getter of the TotalAmountIncluding attribute.
     *
     * @return the total amount with taxes included of the document in Double format.
     */
    @Column(name = "total_amount_including_tax")
    public Double getTotalAmountIncludingTax () {
        return totalAmountIncludingTax;
    }
    
    /**
     * Getter of the DiscountExcludingTax attribute.
     *
     * @return the discount amount without taxes included of the document in Double format.
     */
    @Column(name = "discount_excluding_tax")
    public Double getDiscountExcludingTax () {
        return discountExcludingTax;
    }
    
    /**
     * Getter of the NetTotalExcluding attribute.
     *
     * @return the total amount with discount and without taxes included of the document in Double format.
     */
    @Column(name = "net_total_excluding_tax")
    public Double getNetTotalExcludingTax () {
        return netTotalExcludingTax;
    }
    
    /**
     * Getter of the DiscountIncludingTax attribute.
     *
     * @return the discount amount with taxes included of the document in Double format.
     */
    @Column(name = "discount_including_tax")
    public Double getDiscountIncludingTax () {
        return discountIncludingTax;
    }
    
    /**
     * Setter of the Reference attribute.
     *
     * @param reference - the reference of the document in String format.
     */
    public void setReference (String reference) {
        this.reference = reference;
    }
    
    /**
     * Setter of the Author attribute.
     *
     * @param author - the author of the document in String format.
     */
    public void setAuthor (String author) {
        this.author = author;
    }
    
    /**
     * Setter of the ClientName attribute.
     *
     * @param name
     *         - The name of the client in String format.
     */
    public void setClientName (String name) {
        this.clientName = name;
    }
    
    /**
     * Setter of the ClientAddress attribute.
     *
     * @param address
     *         - The address of the client in String format.
     */
    public void setClientAddress (String address) {
        this.clientAddress = address;
    }
    
    /**
     * Setter of the CompanyLogo attribute.
     *
     * @param value
     *         - The logo of the company in String (SVG) format.
     */
    public void setCompanyLogo (String value) {
        this.companyLogo = value;
    }
    
    /**
     * Setter of the CompanyName attribute.
     *
     * @param value
     *         - The name of the company in String format.
     */
    public void setCompanyName (String value) {
        this.companyName = value;
    }
    
    /**
     * Setter of the CompanyAddress attribute.
     *
     * @param value
     *         - The address of the company in String format.
     */
    public void setCompanyAddress (String value) {
        this.companyAddress = value;
    }
    
    /**
     * Setter of the CompanyDescription attribute.
     *
     * @param value
     *         - The description of the company in String format.
     */
    public void setCompanyDescription (String value) {
        this.companyDescription = value;
    }
    
    /**
     * Setter of the CompanyTelephone attribute.
     *
     * @param value
     *         - The telephone of the company in String format.
     */
    public void setCompanyTelephone (String value) {
        this.companyTelephone = value;
    }
    
    /**
     * Setter of the CompanyEmail attribute.
     *
     * @param value
     *         - The email of the company in String format.
     */
    public void setCompanyEmail (String value) {
        this.companyEmail = value;
    }
    
    /**
     * Setter of the CompanyBankDetails attribute.
     *
     * @param value
     *         - The bank details of the company in String format.
     */
    public void setCompanyBankDetails (String value) {
        this.companyBankDetails = value;
    }
    
    /**
     * Setter of the CompanyInformation attribute.
     *
     * @param value
     *         - The information of the company in String format.
     */
    public void setCompanyInformation (String value) {
        this.companyInformation = value;
    }
    
    /**
     * Setter of the Status attribute.
     *
     * @param status - the status of the document in Enum format.
     */
    public void setStatus (Status status) {
        this.status = status;
    }
    
    /**
     * Setter of the Type attribute.
     *
     * @param type - the type of the document in Enum format.
     */
    public void setType (Type type) {
        this.type = type;
    }
    
    /**
     * Setter of the CreationDate attribute.
     *
     * @param creationDate - the creation date of the document in Timestamp format.
     */
    public void setCreationDate (Timestamp creationDate) {
        this.creationDate = creationDate;
    }
    
    /**
     * Setter of the CreationLocation attribute.
     *
     * @param creationLocation - the creation location of the document in String format.
     */
    public void setCreationLocation (String creationLocation) {
        this.creationLocation = creationLocation;
    }
    
    /**
     * Setter of the UpdateDate attribute.
     *
     * @param updateDate - the update date of the document in Timestamp format.
     */
    public void setUpdateDate (Timestamp updateDate) {
        this.updateDate = updateDate;
    }
    
    /**
     * Setter of the Subject attribute.
     *
     * @param subject - the subject of the document in String format.
     */
    public void setSubject (String subject) {
        this.subject = subject;
    }
    
    /**
     * Setter of the Content attribute.
     *
     * @param content - the content of the document in String format.
     */
    public void setContent (String content) {
        this.content = content;
    }
    
    /**
     * Setter of the ValidityMessage attribute.
     *
     * @param message - the validity message of the document in String format.
     */
    public void setValidity (String message) {
        this.validity = message;
    }
    
    /**
     * Setter of the PaymentMethods attribute.
     *
     * @param paymentMethods - the payment methods of the document in String format.
     */
    public void setPaymentMethods (String paymentMethods) {
        this.paymentMethods = paymentMethods;
    }
    
    /**
     * Setter of the Conditions attribute.
     *
     * @param conditions - the conditions of the document in String format.
     */
    public void setConditions (String conditions) {
        this.conditions = conditions;
    }
    
    /**
     * Setter of the TotalExcludingTax attribute.
     *
     * @param totalAmountExcludingTax - the total excluding tax of the document in double format.
     */
    public void setTotalAmountExcludingTax (double totalAmountExcludingTax) {
        this.totalAmountExcludingTax = totalAmountExcludingTax;
    }
    
    /**
     * Setter of the ValueAddedTax attribute.
     *
     * @param totalVAT - the total amount of the value added tax in the document in double format.
     */
    public void setTotalVAT (double totalVAT) {
        this.totalVAT = totalVAT;
    }
    
    /**
     * Setter of the TotalAllTaxIncluded attribute.
     *
     * @param totalAmountIncludingTax - the total all tax included of the document in double format.
     */
    public void setTotalAmountIncludingTax (double totalAmountIncludingTax) {
        this.totalAmountIncludingTax = totalAmountIncludingTax;
    }
    
    /**
     * Setter of the DiscountExcludingTax attribute.
     *
     * @param discountExcludingTax - the discount amount without taxes of the document in Double format.
     */
    public void setDiscountExcludingTax (Double discountExcludingTax) {
        this.netTotalExcludingTax = discountExcludingTax;
    }
    
    /**
     * Setter of the NetTotalExcluding attribute.
     *
     * @param netTotalExcludingTax
     *         - the net total amount without taxes of the document in Double format.
     */
    public void setNetTotalExcludingTax (Double netTotalExcludingTax) {
        this.netTotalExcludingTax = netTotalExcludingTax;
    }
    
    /**
     * Setter of the DiscountIncludingTax attribute.
     *
     * @param discountIncludingTax
     *         - the discount amount with taxes of the document in Double format.
     */
    public void setDiscountIncludingTax (Double discountIncludingTax) {
        this.discountIncludingTax = discountIncludingTax;
    }
    
    @Override
    public boolean equals (Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof DocumentBean)) {
            return false;
        }
        DocumentBean that = (DocumentBean) o;
        return getReference().equals(that.getReference()) && getAuthor().equals(that.getAuthor()) &&
               getClientName().equals(that.getClientName()) && getClientAddress().equals(that.getClientAddress()) &&
               getCompanyLogo().equals(that.getCompanyLogo()) && getCompanyName().equals(that.getCompanyName()) &&
               getCompanyAddress().equals(that.getCompanyAddress()) &&
               getCompanyDescription().equals(that.getCompanyDescription()) &&
               getCompanyTelephone().equals(that.getCompanyTelephone()) &&
               getCompanyEmail().equals(that.getCompanyEmail()) &&
               getCompanyBankDetails().equals(that.getCompanyBankDetails()) &&
               getCompanyInformation().equals(that.getCompanyInformation()) && getStatus() == that.getStatus() &&
               getType() == that.getType() && getCreationDate().equals(that.getCreationDate()) &&
               getCreationLocation().equals(that.getCreationLocation()) &&
               getUpdateDate().equals(that.getUpdateDate()) && getSubject().equals(that.getSubject()) &&
               getContent().equals(that.getContent()) && getValidity().equals(that.getValidity()) &&
               getPaymentMethods().equals(that.getPaymentMethods()) && getConditions().equals(that.getConditions()) &&
               getTotalAmountExcludingTax().equals(that.getTotalAmountExcludingTax()) &&
               getTotalVAT().equals(that.getTotalVAT()) &&
               getTotalAmountIncludingTax().equals(that.getTotalAmountIncludingTax()) &&
               getDiscountExcludingTax().equals(that.getDiscountExcludingTax()) &&
               getNetTotalExcludingTax().equals(that.getNetTotalExcludingTax()) &&
               getDiscountIncludingTax().equals(that.getDiscountIncludingTax());
    }
    
    @Override
    public int hashCode () {
        return Objects.hash(getReference(),
                            getAuthor(),
                            getClientName(),
                            getClientAddress(),
                            getCompanyLogo(),
                            getCompanyName(),
                            getCompanyAddress(),
                            getCompanyDescription(),
                            getCompanyTelephone(),
                            getCompanyEmail(),
                            getCompanyBankDetails(),
                            getCompanyInformation(),
                            getStatus(),
                            getType(),
                            getCreationDate(),
                            getCreationLocation(),
                            getUpdateDate(),
                            getSubject(),
                            getContent(),
                            getValidity(),
                            getPaymentMethods(),
                            getConditions(),
                            getTotalAmountExcludingTax(),
                            getTotalVAT(),
                            getTotalAmountIncludingTax(),
                            getDiscountExcludingTax(),
                            getNetTotalExcludingTax(),
                            getDiscountIncludingTax());
    }
    
}
