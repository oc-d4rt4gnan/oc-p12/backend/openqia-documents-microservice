package fr.d4rt4gnan.openqia.documents.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Objects;
import java.util.UUID;


/**
 * @author D4RT4GNaN
 * @since 25/05/2022
 */
@Entity
@Table(name = "documents_information", schema = "pgdocuments")
public class DocumentInformationBean {
    
    private UUID   companyId;
    private String validity;
    private String paymentMethods;
    private String conditions;
    
    /**
     * Basic DocumentBean constructor
     */
    public DocumentInformationBean () {
        companyId      = UUID.randomUUID();
        validity       = "";
        paymentMethods = "";
        conditions     = "";
    }
    
    /**
     * Getter of the CompanyId attribute.
     *
     * @return the company id related to the document information.
     */
    @Id
    @Column(name = "company_id")
    public UUID getCompanyId () {
        return companyId;
    }
    
    /**
     * Getter of the ValidityMessage attribute.
     *
     * @return the validity message in the document in String format.
     */
    @Column(name = "validity_message")
    public String getValidity () {
        return validity;
    }
    
    /**
     * Getter of the PaymentMethods attribute.
     *
     * @return the payment methods information in the document in String format.
     */
    @Column(name = "payment_method")
    public String getPaymentMethods () {
        return paymentMethods;
    }
    
    /**
     * Getter of the Conditions attribute.
     *
     * @return the conditions of the document in String format.
     */
    @Column(name = "conditions")
    public String getConditions () {
        return conditions;
    }
    
    /**
     * Setter of the CompanyId attribute.
     *
     * @param companyId
     *         - the company id related to the document information.
     */
    public void setCompanyId (UUID companyId) {
        this.companyId = companyId;
    }
    
    /**
     * Setter of the ValidityMessage attribute.
     *
     * @param message
     *         - the validity message of the document in String format.
     */
    public void setValidity (String message) {
        this.validity = message;
    }
    
    /**
     * Setter of the PaymentMethods attribute.
     *
     * @param paymentMethods
     *         - the payment methods of the document in String format.
     */
    public void setPaymentMethods (String paymentMethods) {
        this.paymentMethods = paymentMethods;
    }
    
    /**
     * Setter of the Conditions attribute.
     *
     * @param conditions
     *         - the conditions of the document in String format.
     */
    public void setConditions (String conditions) {
        this.conditions = conditions;
    }
    
    @Override
    public boolean equals (Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof DocumentInformationBean)) {
            return false;
        }
        DocumentInformationBean that = (DocumentInformationBean) o;
        return getCompanyId().equals(that.getCompanyId()) && getValidity().equals(that.getValidity()) &&
               getPaymentMethods().equals(that.getPaymentMethods()) && getConditions().equals(that.getConditions());
    }
    
    @Override
    public int hashCode () {
        return Objects.hash(getCompanyId(), getValidity(), getPaymentMethods(), getConditions());
    }
    
}
