package fr.d4rt4gnan.openqia.documents.business;


import fr.d4rt4gnan.openqia.documents.api.DocumentsApiDelegate;
import fr.d4rt4gnan.openqia.documents.dao.DocumentsInformationRepository;
import fr.d4rt4gnan.openqia.documents.dao.DocumentsRepository;
import fr.d4rt4gnan.openqia.documents.model.*;
import fr.d4rt4gnan.openqia.documents.technical.mappers.DocumentsInformationMapper;
import fr.d4rt4gnan.openqia.documents.technical.mappers.DocumentsMapper;
import org.openapitools.client.ApiClient;
import org.openapitools.client.ApiException;
import org.openapitools.client.api.CompaniesApi;
import org.openapitools.client.model.Company;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.*;


/**
 * @author D4RT4GNaN
 * @since 14/03/2022
 */
@RestController
public class DocumentsService implements DocumentsApiDelegate {
    
    private DocumentsMapper                documentsMapper;
    private DocumentsRepository            documentsRepository;
    private DocumentsInformationRepository documentsInformationRepository;
    private DocumentsInformationMapper     documentsInformationMapper;
    
    /**
     * Setter of the DocumentRepository attribute.
     *
     * @param documentsRepository - The document repository to be injected.
     */
    @Inject
    public void setDocumentsRepository (
            @Named("documentsRepository")
                    DocumentsRepository documentsRepository
    ) {
        this.documentsRepository = documentsRepository;
    }
    
    /**
     * Setter of the DocumentInformationRepository attribute.
     *
     * @param documentsInformationRepository
     *         - The document information repository to be injected.
     */
    @Inject
    public void setDocumentsInformationRepository (
            @Named("documentsInformationRepository")
                    DocumentsInformationRepository documentsInformationRepository
    ) {
        this.documentsInformationRepository = documentsInformationRepository;
    }
    
    /**
     * Setter of the DocumentMapper attribute.
     *
     * @param documentsMapper - The document mapper to be injected.
     */
    @Inject
    public void setDocumentsMapper (DocumentsMapper documentsMapper) {
        this.documentsMapper = documentsMapper;
    }
    
    /**
     * Setter of the DocumentInformationMapper attribute.
     *
     * @param documentsInformationMapper
     *         - The document information mapper to be injected.
     */
    @Inject
    public void setDocumentsInformationMapper (DocumentsInformationMapper documentsInformationMapper) {
        this.documentsInformationMapper = documentsInformationMapper;
    }
    
    
    
    /**
     * @inheritDoc
     */
    @Override
    public ResponseEntity<Document> addDocument(Document document) {
        // Check Input data
        if (document == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        
        // Convert Document in DocumentBean
        DocumentBean documentBeanConverted = documentsMapper.toDocumentBean(document);
    
        // Generate reference
        if (documentBeanConverted.getReference() == null || documentBeanConverted.getReference().isEmpty()) {
            documentBeanConverted.setReference(this.generateReference(documentBeanConverted.getType()));
        }
        
        // Save DocumentBean
        DocumentBean documentBeanSaved = documentsRepository.save(documentBeanConverted);
        
        // Convert DocumentBean saved into Document and return it
        Document documentSaved = documentsMapper.toDocument(documentBeanSaved);
        return new ResponseEntity<>(documentSaved, HttpStatus.CREATED);
    }
    
    /**
     * @inheritDoc
     */
    @Override
    public ResponseEntity<Document> getDocument (String reference, Type type) {
        // Check validity of the reference in input
        if (reference == null || reference.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        
        // Get the DocumentBean in the database by its reference
        Optional<DocumentBean> dbDocumentBean = documentsRepository.getDocumentBeanByReferenceAndType(reference, type);
        
        // Check if the response contain the DocumentBean else return the NOT_FOUND response
        if (dbDocumentBean.isPresent()) {
            // Convert the result to Document and return it with code 200
            Document document = documentsMapper.toDocument(dbDocumentBean.get());
            return new ResponseEntity<>(document, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
    
    /**
     * @inheritDoc
     */
    @Override
    public ResponseEntity<List<Document>> getDocuments (Type type, Integer page, Integer year, String searchWord) {
        // Check validity of the type in input
        if (type == null || Type.UNDEFINED == type) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    
        // Instantiate the request
        Pageable pageRequest = PageRequest.of(page - 1, 5);
        
        // Get a paginated list of DocumentBeans present in the database and corresponding to the request
        Page<DocumentBean> pagedDocumentBeans;
        if (searchWord == null || searchWord.isEmpty()) {
            pagedDocumentBeans = documentsRepository.findDocumentBeansByTypeAndYear(type.getValue(), year, pageRequest);
        } else {
            if (searchWord.matches("^\\d+$")) {
                pagedDocumentBeans = documentsRepository
                        .findDocumentBeansByTypeAndYearAndPartialReference(type.getValue(),
                                                                           year,
                                                                           searchWord,
                                                                           pageRequest);
            } else {
                pagedDocumentBeans = documentsRepository
                        .findDocumentBeansByTypeAndYearAndPartialClientName(type.getValue(),
                                                                            year,
                                                                            searchWord,
                                                                            pageRequest);
            }
        }
        
        // Check if the result is empty or not
        HttpHeaders responseHeaders = new HttpHeaders();
        if (pagedDocumentBeans.isEmpty()) {
            // Return an empty list of Document with code 200
            List<Document> documents = new ArrayList<>();
            responseHeaders.set("X-Total-Page", "1");
            return ResponseEntity.ok().headers(responseHeaders).body(documents);
        } else {
            // Convert the paginated list of DocumentBean into a list of Document and return it with code 200
            List<Document> documents = documentsMapper.toDocuments(pagedDocumentBeans);
            responseHeaders.set("X-Total-Page", String.format("%d", pagedDocumentBeans.getTotalPages()));
            return ResponseEntity.ok().headers(responseHeaders).body(documents);
        }
    }
    
    /**
     * @inheritDoc
     */
    @Override
    public ResponseEntity<Document> updateDocument(String reference, Document document) {
        // Check validity of the reference and Document object in input
        if (reference == null || reference.isEmpty() || document == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    
        // Get the DocumentBean in the database by its reference
        Optional<DocumentBean> dbDocumentBean = documentsRepository.getDocumentBeanByReferenceAndType(reference,
                                                                                                      document.getType());
    
        // Check if the response contain the DocumentBean else return the NOT_FOUND response
        if (dbDocumentBean.isPresent()) {
            // Update the retrieved DocumentBean object with the Document from frontend, save it, and re-convert the
            // result into Document format to return it with code 200
            DocumentBean documentBeanUpdated = documentsMapper.updateDocumentBean(document, dbDocumentBean.get());
            DocumentBean documentBeanSaved = documentsRepository.save(documentBeanUpdated);
            Document documentConverted = documentsMapper.toDocument(documentBeanSaved);
            return new ResponseEntity<>(documentConverted, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
    
    /**
     * @inheritDoc
     */
    @Override
    public ResponseEntity<String> deleteDocument (String reference, Type type) {
        // Check validity of the reference in input
        if (reference == null || reference.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    
        // Get the DocumentBean in the database by its reference
        Optional<DocumentBean> dbDocumentBean = documentsRepository.getDocumentBeanByReferenceAndType(reference, type);
    
        // Check if the response contain the DocumentBean else return the NOT_FOUND response
        if (dbDocumentBean.isPresent()) {
            // Delete the founded object and return its reference with the code 200
            documentsRepository.delete(dbDocumentBean.get());
            return new ResponseEntity<>(reference, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
    
    /**
     * @inheritDoc
     */
    @Override
    public ResponseEntity<DocumentInformation> getDocumentInformation (
            UUID companyId
    ) {
        Optional<DocumentInformationBean> optionalDocumentInformationBean = this.documentsInformationRepository
                .findDocumentInformationBeanByCompanyId(companyId);
        if (optionalDocumentInformationBean.isPresent()) {
            DocumentInformation documentInformation = this.documentsInformationMapper.toDocumentInformation(
                    optionalDocumentInformationBean.get());
            return new ResponseEntity<>(documentInformation, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
    
    /**
     * @inheritDoc
     */
    @Override
    public ResponseEntity<DocumentInformation> saveDocumentInformation (
            UUID companyId, DocumentInformation documentInformation
    ) {
        Optional<DocumentInformationBean> optionalDocumentInformationBean = this.documentsInformationRepository
                .findDocumentInformationBeanByCompanyId(companyId);
        
        DocumentInformationBean documentInformationBean;
        if (optionalDocumentInformationBean.isPresent()) {
            documentInformationBean =
                    this.documentsInformationMapper.update(optionalDocumentInformationBean.get(), documentInformation);
        } else {
            documentInformationBean =
                    this.documentsInformationMapper.toDocumentInformationBean(documentInformation);
        }
    
        DocumentInformationBean documentInformationBeanSaved =
                this.documentsInformationRepository.save(documentInformationBean);
        DocumentInformation documentInformationSaved =
                this.documentsInformationMapper.toDocumentInformation(documentInformationBeanSaved);
        return new ResponseEntity<>(documentInformationSaved, HttpStatus.OK);
    }
    
    /**
     * @inheritDoc
     */
    @Override
    public ResponseEntity<Document> getNewDocument (Type type) {
        ApiClient client = new ApiClient();
        CompaniesApi api = new CompaniesApi(client);
        Document document = new Document();
        List<Company> companies = null;
        try {
            companies = api.getCompanies(0, 1);
        } catch (ApiException e) {
            e.printStackTrace();
        }
        if (companies != null) {
            String newReference = this.generateReference(type);
            Company company = companies.get(0);
            DocumentInformation documentInformation = null;
            Optional<DocumentInformationBean> optionalDocumentInformationBean = this.documentsInformationRepository
                    .findDocumentInformationBeanByCompanyId(company.getId());
            if (optionalDocumentInformationBean.isPresent()) {
                documentInformation = this.documentsInformationMapper.toDocumentInformation(
                        optionalDocumentInformationBean.get());
            }
            document = documentsMapper.setNewDocument(newReference, company, documentInformation, type);
        }
        return new ResponseEntity<>(document, HttpStatus.OK);
    }
    
    /**
     * Generate new unique reference
     *
     * @param type
     *         - the type of the document to set the reference
     *
     * @return the new generated reference
     */
    private String generateReference (Type type) {
        Calendar now = new GregorianCalendar();
        String yearString = String.format("%d", now.get(Calendar.YEAR));
        int month = now.get(Calendar.MONTH) + 1;
        String monthString = month < 10 ? String.format("0%d", month) : String.format("%d", month);
        String lastReference = documentsRepository.getLastReferenceByType(type.getValue());
        if (lastReference == null || lastReference.isEmpty()) {
            return String.format("%s%s001", yearString, monthString);
        } else {
            String date = lastReference.substring(0, 6);
            String nowDate = String.format("%s%s", yearString, monthString);
            if (date.equals(nowDate)) {
                return String.format("%d", Integer.parseInt(lastReference) + 1);
            } else {
                return String.format("%s001", nowDate);
            }
        }
    }
}