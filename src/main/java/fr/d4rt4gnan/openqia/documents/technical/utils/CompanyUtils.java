package fr.d4rt4gnan.openqia.documents.technical.utils;


import org.openapitools.client.model.Company;


/**
 * @author D4RT4GNaN
 * @since 06/06/2022
 */
public interface CompanyUtils {
    
    /**
     * Allows you to retrieve the city in the address.
     *
     * @param address - The address containing the city name.
     * @return the city name in the address.
     */
    public String getCity (String address);
    
    
    
    /**
     * Allows you to return the full name of the user belonging to the company.
     *
     * @param company - The company object containing the first name and last name.
     * @return the full name of the company user, in one string.
     */
    public String getAuthorFullName (Company company);
    
}
