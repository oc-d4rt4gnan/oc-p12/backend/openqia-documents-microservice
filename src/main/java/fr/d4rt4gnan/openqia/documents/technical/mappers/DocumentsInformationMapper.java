package fr.d4rt4gnan.openqia.documents.technical.mappers;


import fr.d4rt4gnan.openqia.documents.model.DocumentInformation;
import fr.d4rt4gnan.openqia.documents.model.DocumentInformationBean;


/**
 * @author D4RT4GNaN
 * @since 16/03/2022
 */
public interface DocumentsInformationMapper {
    
    /**
     * Convert a document information in database version from the frontend version.
     *
     * @param documentInformation
     *         - Input document information from frontend.
     *
     * @return the document in database version.
     */
    DocumentInformationBean toDocumentInformationBean (DocumentInformation documentInformation);
    
    
    
    /**
     * Convert a document information in frontend version from the database version.
     *
     * @param documentInformationBean
     *         - Input document information from database.
     *
     * @return the document in frontend version.
     */
    DocumentInformation toDocumentInformation (DocumentInformationBean documentInformationBean);
    
    
    
    /**
     * Update the DocumentInformationBean in parameter with data from the DocumentInformation in parameter too.
     *
     * @param documentInformationBean - The document information from database corresponding to the company id of the
     *                               DocumentInformation object passed in parameter.
     * @param documentInformation - The new document information data.
     * @return an DocumentInformationBean updated with the data from DocumentInformation.
     */
    DocumentInformationBean update (DocumentInformationBean documentInformationBean, DocumentInformation documentInformation);
    
}
