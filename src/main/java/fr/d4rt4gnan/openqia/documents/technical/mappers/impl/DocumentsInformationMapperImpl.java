package fr.d4rt4gnan.openqia.documents.technical.mappers.impl;


import fr.d4rt4gnan.openqia.documents.model.DocumentInformation;
import fr.d4rt4gnan.openqia.documents.model.DocumentInformationBean;
import fr.d4rt4gnan.openqia.documents.technical.mappers.DocumentsInformationMapper;

import javax.inject.Named;


/**
 * @author D4RT4GNaN
 * @since 25/05/2022
 */
@Named("documentsInformationMapper")
public class DocumentsInformationMapperImpl implements DocumentsInformationMapper {
    
    /**
     * @inheritDoc
     */
    @Override
    public DocumentInformationBean toDocumentInformationBean (
            DocumentInformation documentInformation
    ) {
        DocumentInformationBean documentInformationBean = new DocumentInformationBean();
        documentInformationBean.setCompanyId(documentInformation.getCompanyId());
        documentInformationBean = this.update(documentInformationBean, documentInformation);
        
        return documentInformationBean;
    }
    
    /**
     * @inheritDoc
     */
    @Override
    public DocumentInformation toDocumentInformation (DocumentInformationBean documentInformationBean) {
        DocumentInformation documentInformation = new DocumentInformation();
        documentInformation.setCompanyId(documentInformationBean.getCompanyId());
        documentInformation.setValidityMessage(documentInformationBean.getValidity());
        documentInformation.setPaymentMethod(documentInformationBean.getPaymentMethods());
        documentInformation.setConditions(documentInformationBean.getConditions());
        
        return documentInformation;
    }
    
    /**
     * @inheritDoc
     */
    @Override
    public DocumentInformationBean update (
            DocumentInformationBean documentInformationBean, DocumentInformation documentInformation
    ) {
        documentInformationBean.setValidity(documentInformation.getValidityMessage());
        documentInformationBean.setPaymentMethods(documentInformation.getPaymentMethod());
        documentInformationBean.setConditions(documentInformation.getConditions());
        
        return documentInformationBean;
    }
    
}
