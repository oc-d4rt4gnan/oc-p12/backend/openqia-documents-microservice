package fr.d4rt4gnan.openqia.documents.technical.converters;


import fr.d4rt4gnan.openqia.documents.model.Type;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import static java.util.Optional.ofNullable;


@Converter(autoApply = true)
public class TypeConverter implements AttributeConverter<Type, String> {
    
    @Override
    public String convertToDatabaseColumn (Type type) {
        return ofNullable(type)
                .map(Type::getValue)
                .orElse(Type.UNDEFINED.getValue());
    }
    
    @Override
    public Type convertToEntityAttribute (String type) {
        return ofNullable(type)
                .map(Type::fromValue)
                .orElse(Type.UNDEFINED);
    }
    
}
