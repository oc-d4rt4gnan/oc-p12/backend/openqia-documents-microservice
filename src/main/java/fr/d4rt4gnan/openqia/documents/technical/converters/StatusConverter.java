package fr.d4rt4gnan.openqia.documents.technical.converters;


import fr.d4rt4gnan.openqia.documents.model.Status;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import static java.util.Optional.ofNullable;


@Converter(autoApply = true)
public class StatusConverter implements AttributeConverter<Status, String> {
    
    @Override
    public String convertToDatabaseColumn (Status status) {
        return ofNullable(status)
                .map(Status::getValue)
                .orElse(Status.PENDING.getValue());
    }
    
    @Override
    public Status convertToEntityAttribute (String status) {
        return ofNullable(status)
                .map(Status::fromValue)
                .orElse(Status.PENDING);
    }
    
}
