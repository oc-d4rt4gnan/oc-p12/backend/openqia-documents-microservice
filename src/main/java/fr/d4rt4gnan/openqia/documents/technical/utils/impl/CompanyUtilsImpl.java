package fr.d4rt4gnan.openqia.documents.technical.utils.impl;


import fr.d4rt4gnan.openqia.documents.technical.utils.CompanyUtils;
import org.openapitools.client.model.Company;

import javax.inject.Named;


/**
 * @author D4RT4GNaN
 * @since 06/06/2022
 */
@Named("companyUtils")
public class CompanyUtilsImpl implements CompanyUtils {
    
    /**
     * @inheritDoc
     */
    @Override
    public String getCity (String address) {
        if (address != null && !address.isEmpty() && address.contains(",")) {
            return address.split(",")[1].replaceAll("[\\d\\s]", "");
        } else {
            return "";
        }
    }
    
    /**
     * @inheritDoc
     */
    @Override
    public String getAuthorFullName (Company company) {
        if (company == null) {
            return "";
        } else {
            return String.format("%s %s", company.getFirstName(), company.getLastName());
        }
    }
    
}
