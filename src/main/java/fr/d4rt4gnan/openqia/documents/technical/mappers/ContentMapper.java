package fr.d4rt4gnan.openqia.documents.technical.mappers;


import fr.d4rt4gnan.openqia.documents.model.Section;

import java.util.List;


/**
 * @author D4RT4GNaN
 * @since 19/03/2022
 */
public interface ContentMapper {
    
    /**
     * Serialize in JSON format the content from a list of section containing the lines of articles.
     *
     * @param content - The list of articles in each section.
     * @return the content in JSON format.
     */
    String toString(List<Section> content);
    
    
    
    /**
     * Deserialize the content from JSON to list of section.
     *
     * @param content - The list of articles in JSON.
     * @return the list of section with ours list of articles.
     */
    List<Section> toListSection (String content);
    
}
