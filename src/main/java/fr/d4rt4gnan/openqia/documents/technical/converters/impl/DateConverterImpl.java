package fr.d4rt4gnan.openqia.documents.technical.converters.impl;


import fr.d4rt4gnan.openqia.documents.technical.converters.DateConverter;

import javax.inject.Named;
import java.sql.Timestamp;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;


/**
 * @author D4RT4GNaN
 * @since 19/03/2022
 */
@Named("dateConverter")
public class DateConverterImpl implements DateConverter {
    
    /**
     * @inheritDoc
     */
    @Override
    public OffsetDateTime toOffsetDateTime (Timestamp timestamp) {
        return OffsetDateTime.of(timestamp.toLocalDateTime(), ZoneOffset.UTC);
    }
    
    /**
     * @inheritDoc
     */
    @Override
    public Timestamp toTimestamp (final OffsetDateTime offsetDateTime) {
        return Timestamp.valueOf(offsetDateTime.atZoneSameInstant(ZoneOffset.UTC).toLocalDateTime());
    }
    
}
