package fr.d4rt4gnan.openqia.documents.technical.converters;


import java.sql.Timestamp;
import java.time.OffsetDateTime;


/**
 * @author D4RT4GNaN
 * @since 19/03/2022
 */
public interface DateConverter {
    
    /**
     * Convert date from Timestamp format to OffsetDateTime format.
     *
     * @param timestamp - The date in Timestamp format.
     * @return the date in OffsetDateTime format.
     */
    OffsetDateTime toOffsetDateTime (Timestamp timestamp);
    
    
    
    /**
     * Convert date from OffsetDateTime format to Timestamp format.
     *
     * @param offsetDateTime - The date in OffsetDateTime format.
     * @return the date in Timestamp format.
     */
    Timestamp toTimestamp(OffsetDateTime offsetDateTime);
    
}
