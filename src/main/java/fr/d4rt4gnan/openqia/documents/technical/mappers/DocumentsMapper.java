package fr.d4rt4gnan.openqia.documents.technical.mappers;


import fr.d4rt4gnan.openqia.documents.model.Document;
import fr.d4rt4gnan.openqia.documents.model.DocumentBean;
import fr.d4rt4gnan.openqia.documents.model.DocumentInformation;
import fr.d4rt4gnan.openqia.documents.model.Type;
import org.openapitools.client.model.Company;
import org.springframework.data.domain.Page;

import java.util.List;


/**
 * @author D4RT4GNaN
 * @since 25/05/2022
 */
public interface DocumentsMapper {
    
    /**
     * Convert a document in database version from the frontend version.
     *
     * @param document - Input document from frontend.
     * @return the document in database version.
     */
    DocumentBean toDocumentBean(Document document);
    
    
    
    /**
     * Convert a document in frontend version from the database version.
     *
     * @param documentBean - Input document from database.
     * @return the document in frontend version.
     */
    Document toDocument(DocumentBean documentBean);
    
    
    
    /**
     * Convert a list of document in frontend version from the database version.
     *
     * @param pagedDocumentBeans - The paginated list of DocumentBean from the database.
     * @return the list of documents in frontend version.
     */
    List<Document> toDocuments (Page<DocumentBean> pagedDocumentBeans);
    
    
    
    /**
     * Allow to pass updated value from the front Document object to the database DocumentBean object.
     *
     * @param document - The front Document object containing the updated value.
     * @param documentBean - The database DocumentBean object to be updated.
     * @return the updated Document object.
     */
    DocumentBean updateDocumentBean (Document document, DocumentBean documentBean);
    
    
    /**
     * Fill in the company information in a new document.
     *
     * @param reference - The new reference.
     * @param company - The object containing the company's information.
     * @param documentInformation - The object containing the document's information.
     * @param type - The type of document (invoice or estimate).
     * @return a document pre-filled with the company's information.
     */
    Document setNewDocument (String reference, Company company, DocumentInformation documentInformation, Type type);
}
