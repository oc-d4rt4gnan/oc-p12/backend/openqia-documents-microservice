package fr.d4rt4gnan.openqia.documents.technical.mappers.impl;


import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import fr.d4rt4gnan.openqia.documents.model.Section;
import fr.d4rt4gnan.openqia.documents.technical.mappers.ContentMapper;

import javax.inject.Named;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;


/**
 * @author D4RT4GNaN
 * @since 19/03/2022
 */
@Named("contentMapper")
public class ContentMapperImpl implements ContentMapper {
    
    /**
     * @inheritDoc
     */
    @Override
    public String toString (List<Section> content) {
        return new Gson().toJson(content);
    }
    
    /**
     * @inheritDoc
     */
    @Override
    public List<Section> toListSection (String content) {
        Type listType = new TypeToken<ArrayList<Section>>(){}.getType();
        return new Gson().fromJson(content, listType);
    }
    
}
