package fr.d4rt4gnan.openqia.documents.technical.mappers.impl;


import fr.d4rt4gnan.openqia.documents.model.*;
import fr.d4rt4gnan.openqia.documents.model.Document;
import fr.d4rt4gnan.openqia.documents.model.DocumentBean;
import fr.d4rt4gnan.openqia.documents.model.Section;
import fr.d4rt4gnan.openqia.documents.technical.converters.DateConverter;
import fr.d4rt4gnan.openqia.documents.technical.mappers.ContentMapper;
import fr.d4rt4gnan.openqia.documents.technical.mappers.DocumentsMapper;
import fr.d4rt4gnan.openqia.documents.technical.utils.CompanyUtils;
import fr.d4rt4gnan.openqia.documents.technical.utils.impl.CompanyUtilsImpl;
import org.openapitools.client.model.Company;
import org.springframework.data.domain.Page;

import javax.inject.Inject;
import javax.inject.Named;
import java.sql.Timestamp;
import java.time.OffsetDateTime;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;


/**
 * @author D4RT4GNaN
 * @since 16/03/2022
 */
@Named("documentsMapper")
public class DocumentsMapperImpl implements DocumentsMapper {

    private ContentMapper contentMapper;
    private DateConverter dateConverter;
    private CompanyUtils  companyUtils;
    
    /**
     * Setter of the Content Mapper.
     *
     * @param contentMapper - The content mapper instance to be injected.
     */
    @Inject
    public void setContentMapper (ContentMapper contentMapper) {
        this.contentMapper = contentMapper;
    }
    
    /**
     * Setter of the Date Converter.
     *
     * @param dateConverter - The date converter instance to be injected.
     */
    @Inject
    public void setDateConverter (DateConverter dateConverter) {
        this.dateConverter = dateConverter;
    }
    
    /**
     * Setter of the Company Utils.
     *
     * @param companyUtils - The company utils instance to be injected.
     */
    @Inject
    public void setCompanyUtils (CompanyUtilsImpl companyUtils) {
        this.companyUtils = companyUtils;
    }
    
    /**
     * @inheritDoc
     */
    @Override
    public DocumentBean toDocumentBean (
            Document document
    ) {
        // Mapping
        DocumentBean documentBean = new DocumentBean();
        documentBean.setReference(document.getReference());
        
        return toDocumentBean(document, documentBean);
    }
    
    /**
     * @inheritDoc
     */
    @Override
    public Document toDocument (DocumentBean documentBean) {
        // Date conversion
        OffsetDateTime creationDate = dateConverter.toOffsetDateTime(documentBean.getCreationDate());
        OffsetDateTime updateDate = dateConverter.toOffsetDateTime(documentBean.getUpdateDate());
    
        // Mapping
        List<Section> content = contentMapper.toListSection(documentBean.getContent());
        
        Document document = new Document();
        document.setReference(documentBean.getReference());
        document.setAuthor(documentBean.getAuthor());
        document.setClientName(documentBean.getClientName());
        document.setClientAddress(documentBean.getClientAddress());
        document.setCompanyName(documentBean.getCompanyName());
        document.setCompanyAddress(documentBean.getCompanyAddress());
        document.setCompanyLogoSVG(documentBean.getCompanyLogo());
        document.setCompanyDescription(documentBean.getCompanyDescription());
        document.setCompanyEmail(documentBean.getCompanyEmail());
        document.setCompanyTelephone(documentBean.getCompanyTelephone());
        document.setCompanyBankDetails(documentBean.getCompanyBankDetails());
        document.setCompanyInformation(documentBean.getCompanyInformation());
        document.setCreationDate(creationDate);
        document.setUpdateDate(updateDate);
        document.setCreationLocation(documentBean.getCreationLocation());
        document.setStatus(documentBean.getStatus());
        document.setType(documentBean.getType());
        document.setSubjet(documentBean.getSubject());
        document.setContent(content);
        document.setValidityMessage(documentBean.getValidity());
        document.setPaymentMethod(documentBean.getPaymentMethods());
        document.setConditions(documentBean.getConditions());
        document.setTotalAmountExcludingTax(documentBean.getTotalAmountExcludingTax());
        document.setTotalVAT(documentBean.getTotalVAT());
        document.setTotalAmountIncludingTax(documentBean.getTotalAmountIncludingTax());
        document.setDiscountExcludingTax(documentBean.getDiscountExcludingTax());
        document.setNetTotalExcludingTax(documentBean.getNetTotalExcludingTax());
        document.setDiscountIncludingTax(documentBean.getDiscountIncludingTax());
        
        return document;
    }
    
    /**
     * @inheritDoc
     */
    @Override
    public List<Document> toDocuments(Page<DocumentBean> pagedDocumentBeans) {
        return pagedDocumentBeans.stream().map(this::toDocument).collect(Collectors.toList());
    }
    
    /**
     * @inheritDoc
     */
    @Override
    public DocumentBean updateDocumentBean (
            Document document,
            DocumentBean documentBean
    ) {
        return toDocumentBean(document, documentBean);
    }
    
    private DocumentBean toDocumentBean(Document document, DocumentBean documentBean) {
        // Mapping inner bean
        String content = contentMapper.toString(document.getContent());
    
        // Date conversion
        Timestamp creationDate = dateConverter.toTimestamp(document.getCreationDate());
        Timestamp updateDate = dateConverter.toTimestamp(document.getUpdateDate());
    
        // Mapping
        documentBean.setAuthor(document.getAuthor());
        documentBean.setClientName(document.getClientName());
        documentBean.setClientAddress(document.getClientAddress());
        documentBean.setCompanyLogo(document.getCompanyLogoSVG());
        documentBean.setCompanyName(document.getCompanyName());
        documentBean.setCompanyAddress(document.getCompanyAddress());
        documentBean.setCompanyDescription(document.getCompanyDescription());
        documentBean.setCompanyEmail(document.getCompanyEmail());
        documentBean.setCompanyTelephone(document.getCompanyTelephone());
        documentBean.setCompanyBankDetails(document.getCompanyBankDetails());
        documentBean.setCompanyInformation(document.getCompanyInformation());
        documentBean.setCreationDate(creationDate);
        documentBean.setUpdateDate(updateDate);
        documentBean.setCreationLocation(document.getCreationLocation());
        documentBean.setStatus(document.getStatus());
        documentBean.setType(document.getType());
        documentBean.setSubject(document.getSubjet());
        documentBean.setContent(content);
        documentBean.setValidity(document.getValidityMessage());
        documentBean.setPaymentMethods(document.getPaymentMethod());
        documentBean.setConditions(document.getConditions());
        documentBean.setTotalAmountExcludingTax(document.getTotalAmountExcludingTax());
        documentBean.setTotalVAT(document.getTotalVAT());
        documentBean.setTotalAmountIncludingTax(document.getTotalAmountIncludingTax());
        documentBean.setDiscountExcludingTax(document.getDiscountExcludingTax());
        documentBean.setNetTotalExcludingTax(document.getNetTotalExcludingTax());
        documentBean.setDiscountIncludingTax(document.getDiscountIncludingTax());
    
        return documentBean;
    }
    
    /**
     * @inheritDoc
     */
    @Override
    public Document setNewDocument (String reference, Company company, DocumentInformation documentInformation, Type type) {
        Document document = new Document();
        OffsetDateTime date = OffsetDateTime.now();
        document.setReference(reference);
        document.setType(type);
        document.setStatus(Status.PENDING);
        document.setUpdateDate(date);
        document.setCreationDate(date);
        document.setContent(this.setEmptyContent());
        document.setTotalVAT(0.0);
        document.setTotalAmountExcludingTax(0.0);
        document.setTotalAmountIncludingTax(0.0);
        
        if (company != null) {
            document.setCompanyInformation(company.getOtherInformation());
            document.setCompanyBankDetails(company.getBankDetail());
            document.setCompanyTelephone(company.getTelephone());
            document.setCompanyEmail(company.getEmail());
            document.setCompanyLogoSVG(company.getLogoSVG());
            document.setCompanyDescription(company.getDescription());
            document.setCompanyAddress(company.getAddress());
            document.setCompanyName(company.getName());
            document.setCreationLocation(companyUtils.getCity(company.getAddress()));
            document.setAuthor(companyUtils.getAuthorFullName(company));
        }
        
        if (documentInformation != null) {
            document.setValidityMessage(documentInformation.getValidityMessage());
            document.setConditions(documentInformation.getConditions());
            document.setPaymentMethod(documentInformation.getPaymentMethod());
        }
        return document;
    }
    
    private List<Section> setEmptyContent () {
        Article article = new Article();
        List<Article> articleList = Collections.singletonList(article);
        Section section = new Section();
        section.setContent(articleList);
        return Collections.singletonList(section);
    }
    
}
