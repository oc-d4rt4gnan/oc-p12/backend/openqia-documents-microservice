package fr.d4rt4gnan.openqia.documents.dao;


import fr.d4rt4gnan.openqia.documents.model.DocumentInformationBean;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.inject.Named;
import java.util.Optional;
import java.util.UUID;


/**
 * @author D4RT4GNaN
 * @since 14/03/2022
 */
@Repository
@Named("documentsInformationRepository")
public interface DocumentsInformationRepository extends JpaRepository<DocumentInformationBean, UUID> {
    
    /**
     * Get document information by the associated company id.
     *
     * @param companyId
     *         - The associated company id of the document information in search.
     *
     * @return the document information associated to the company id passed in parameter.
     */
    Optional<DocumentInformationBean> findDocumentInformationBeanByCompanyId (UUID companyId);
    
}
