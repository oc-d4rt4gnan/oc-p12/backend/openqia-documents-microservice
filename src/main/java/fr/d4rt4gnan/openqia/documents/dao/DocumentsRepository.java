package fr.d4rt4gnan.openqia.documents.dao;


import fr.d4rt4gnan.openqia.documents.model.DocumentBean;
import fr.d4rt4gnan.openqia.documents.model.Type;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.inject.Named;
import java.util.List;
import java.util.Optional;


/**
 * @author D4RT4GNaN
 * @since 14/03/2022
 */
@Repository
@Named("documentsRepository")
public interface DocumentsRepository extends PagingAndSortingRepository<DocumentBean, String> {
    
    /**
     * Getter of an optional DocumentBean by its reference.
     *
     * @param reference - The reference of the DocumentBean in search.
     * @param type - The type of the DocumentBean in search.
     * @return an optional with the DocumentBean found if there is it.
     */
    Optional<DocumentBean> getDocumentBeanByReferenceAndType (String reference, Type type);
    
    
    
    /**
     * Retrieve documents of a specified type and return it in paginated format.
     *
     * @param type - The type of the document searched.
     * @param year - The year of the documents in search.
     * @param pageable - The parameter of the pagination.
     * @return documents founded in paginated format.
     */
    @Query(value = "SELECT * FROM pgdocuments.documents d WHERE d.document_type= :document_type AND EXTRACT(YEAR FROM" +
                   " d.creation_date)=:year", nativeQuery = true)
    Page<DocumentBean> findDocumentBeansByTypeAndYear (
            @Param("document_type")
                    String type,
            @Param("year")
                    Integer year, Pageable pageable
    );
    
    
    
    /**
     * Getter of the last document reference by its type.
     *
     * @param type
     *         - The type of the DocumentBean in search.
     *
     * @return the last document reference of type sent in parameter if there is it.
     */
    @Query(value = "SELECT reference FROM pgdocuments.documents d WHERE d.document_type= :document_type ORDER BY d" +
                   ".reference DESC LIMIT 1", nativeQuery = true)
    String getLastReferenceByType (
            @Param("document_type")
                    String type
    );
    
    
    
    /**
     * Getter of the documents different years by ours type.
     *
     * @param type
     *         - The type of the documents.
     *
     * @return the documents different years of a type sent in parameter if there is it.
     */
    @Query(value = "SELECT DISTINCT EXTRACT(YEAR FROM creation_date) from pgdocuments.documents d" +
                   " where d.document_type= :document_type", nativeQuery = true)
    List<String> getAllYearsByType (
            @Param("document_type")
                    String type
    );
    
    
    
    /**
     * Getter of the documents different years by ours type and partial reference.
     *
     * @param type
     *         - The type of the document searched.
     * @param year
     *         - The year of the documents in search.
     * @param partialReference
     *         - The partial reference in search.
     * @param pageable
     *         - The parameter of the pagination.
     *
     * @return the documents different years of a type sent in parameter if there is it.
     */
    @Query(value = "SELECT * FROM (SELECT * FROM pgdocuments.documents d WHERE d.document_type= :document_type AND " +
                   "EXTRACT(YEAR FROM d.creation_date)=:year) filtered WHERE filtered.reference LIKE " +
                   "CONCAT('%', :partial_reference, '%')",
           nativeQuery = true)
    Page<DocumentBean> findDocumentBeansByTypeAndYearAndPartialReference (
            @Param("document_type")
                    String type,
            @Param("year")
                    Integer year,
            @Param("partial_reference")
                    String partialReference, Pageable pageable
    );
    
    
    
    /**
     * Getter of the documents different years by ours type and partial client name.
     *
     * @param type
     *         - The type of the document searched.
     * @param year
     *         - The year of the documents in search.
     * @param partialClientName
     *         - The partial client name in search.
     * @param pageable
     *         - The parameter of the pagination.
     *
     * @return the documents different years of a type sent in parameter if there is it.
     */
    @Query(value = "SELECT * FROM (SELECT * FROM pgdocuments.documents d WHERE d.document_type= :document_type AND " +
                   "EXTRACT(YEAR FROM d.creation_date)=:year) filtered WHERE filtered.client_name LIKE " +
                   "CONCAT('%', :partial_client_name, '%')",
           nativeQuery = true)
    Page<DocumentBean> findDocumentBeansByTypeAndYearAndPartialClientName (
            @Param("document_type")
                    String type,
            @Param("year")
                    Integer year,
            @Param("partial_client_name")
                    String partialClientName, Pageable pageable
    );
    
}
