package fr.d4rt4gnan.openqia.documents.technical.mappers;


import fr.d4rt4gnan.openqia.documents.model.Article;
import fr.d4rt4gnan.openqia.documents.model.Section;
import fr.d4rt4gnan.openqia.documents.technical.mappers.impl.ContentMapperImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;


/**
 * @author D4RT4GNaN
 * @since 25/03/2022
 */
class ContentMapperTest {
    
    private ContentMapper classUnderTest = new ContentMapperImpl();
    
    private List<Section> objectContent;
    private String        jsonContent;
    
    @BeforeEach
    void init() {
        Article article = new Article();
        article.setDesignation("designation");
        article.setUnitPrice(16.90);
        article.setQuantity(1);
    
        Section section = new Section();
        section.setName("section");
        section.setContent(Collections.singletonList(article));
    
        objectContent = Collections.singletonList(section);
    
        jsonContent =
                "[" +
                    "{" +
                        "\"name\":\"section\"," +
                        "\"content\":[" +
                            "{" +
                                "\"designation\":\"designation\"," +
                                "\"quantity\":1," +
                                "\"unitPrice\":16.9" +
                            "}" +
                        "]" +
                    "}" +
                "]";
    }
    
    @Test
    void givenAListOfSection_whenToStringIsCalled_thenItReturnTheContentInJSONFormat () {
        // GIVEN
        String expected = jsonContent;
        
        // WHEN
        String result = classUnderTest.toString(objectContent);
        
        // THEN
        assertThat(result).isEqualTo(expected);
    }
    
    @Test
    void givenAJSON_whenToListSectionIsCalled_thenItReturnTheContentInListOfSectionFormat () {
        // GIVEN
        List<Section> expected = objectContent;
        
        // WHEN
        List<Section> result = classUnderTest.toListSection(jsonContent);
        
        // THEN
        assertThat(result).usingRecursiveComparison().isEqualTo(expected);
    }
    
}
