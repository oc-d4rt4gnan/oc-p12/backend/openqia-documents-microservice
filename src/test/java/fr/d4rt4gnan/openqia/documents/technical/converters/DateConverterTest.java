package fr.d4rt4gnan.openqia.documents.technical.converters;


import fr.d4rt4gnan.openqia.documents.technical.converters.impl.DateConverterImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;

import static org.assertj.core.api.Assertions.assertThat;


/**
 * @author D4RT4GNaN
 * @since 25/03/2022
 */
class DateConverterTest {
    
    private DateConverter classUnderTest = new DateConverterImpl();
    
    private Timestamp timestamp;
    private OffsetDateTime offsetDateTime;

    @BeforeEach
    void init() {
        LocalDateTime now = LocalDateTime.now();
        timestamp = Timestamp.valueOf(now);
        offsetDateTime = OffsetDateTime.of(now, ZoneOffset.UTC);
    }
    
    @Test
    void givenATimestampDate_whenToOffsetDateTimeIsCalled_thenItReturnCorrespondingDateInOffsetDateTimeFormat () {
        // GIVEN
        OffsetDateTime expected = offsetDateTime;
        
        // WHEN
        OffsetDateTime result = classUnderTest.toOffsetDateTime(timestamp);
        
        // THEN
        assertThat(result).isEqualTo(expected);
    }
    
    @Test
    void givenAnOffsetDateTime_whenToTimestampIsCalled_thenItReturnCorrespondingDateInTimestampFormat () {
        // GIVEN
        Timestamp expected = timestamp;
        
        // WHEN
        Timestamp result = classUnderTest.toTimestamp(offsetDateTime);
        
        // THEN
        assertThat(result).isEqualTo(expected);
    }
    
}
