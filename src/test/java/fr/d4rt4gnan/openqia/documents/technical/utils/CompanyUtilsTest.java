package fr.d4rt4gnan.openqia.documents.technical.utils;


import fr.d4rt4gnan.openqia.documents.technical.utils.impl.CompanyUtilsImpl;
import org.junit.jupiter.api.Test;
import org.openapitools.client.model.Company;

import static org.assertj.core.api.Assertions.assertThat;


/**
 * @author D4RT4GNaN
 * @since 06/06/2022
 */
class CompanyUtilsTest {
    
    private CompanyUtils classUnderTest = new CompanyUtilsImpl();
    
    @Test
    void givenCompanyContainingFirstNameAnoAndLastNameNymous_whenGetAuthorFullNameIsCalled_thenItReturnAnoNymous () {
        // GIVEN
        Company company = new Company();
        company.setFirstName("Ano");
        company.setLastName("Nymous");
        String expected = "Ano Nymous";
        
        // WHEN
        String result = classUnderTest.getAuthorFullName(company);
        
        // THEN
        assertThat(result).isEqualTo(expected);
    }
    
    @Test
    void givenNullCompany_whenGetAuthorFullNameIsCalled_thenItReturnAnEmptyString () {
        // GIVEN
        String expected = "";
        
        // WHEN
        String result = classUnderTest.getAuthorFullName(null);
        
        // THEN
        assertThat(result).isEqualTo(expected);
    }
    
    @Test
    void givenANullAddress_whenGetCityIsCalled_thenItReturnAnEmptyString () {
        // GIVEN
        String expected = "";
        
        // WHEN
        String result = classUnderTest.getCity(null);
        
        // THEN
        assertThat(result).isEqualTo(expected);
    }
    
    @Test
    void givenAnEmptyAddress_whenGetCityIsCalled_thenItReturnAnEmptyString () {
        // GIVEN
        String expected = "";
        
        // WHEN
        String result = classUnderTest.getCity("");
        
        // THEN
        assertThat(result).isEqualTo(expected);
    }
    
    @Test
    void givenAnAddressWithoutComma_whenGetCityIsCalled_thenItReturnAnEmptyString () {
        // GIVEN
        String address = "1789 Gore Street 75163 Houston";
        String expected = "";
        
        // WHEN
        String result = classUnderTest.getCity(address);
        
        // THEN
        assertThat(result).isEqualTo(expected);
    }
    
    @Test
    void givenAValidAddress_whenGetCityIsCalled_thenItReturnTheCityName () {
        // GIVEN
        String address = "1789 Gore Street, 75163 Houston";
        String expected = "Houston";
        
        // WHEN
        String result = classUnderTest.getCity(address);
        
        // THEN
        assertThat(result).isEqualTo(expected);
    }
    
}
