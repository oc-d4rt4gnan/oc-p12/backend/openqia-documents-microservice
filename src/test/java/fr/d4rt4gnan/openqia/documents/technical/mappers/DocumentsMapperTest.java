package fr.d4rt4gnan.openqia.documents.technical.mappers;


import fr.d4rt4gnan.openqia.documents.model.*;
import fr.d4rt4gnan.openqia.documents.technical.converters.DateConverter;
import fr.d4rt4gnan.openqia.documents.technical.mappers.impl.DocumentsMapperImpl;
import fr.d4rt4gnan.openqia.documents.technical.utils.CompanyUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.openapitools.client.model.Company;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.sql.Timestamp;
import java.time.OffsetDateTime;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;


/**
 * @author D4RT4GNaN
 * @since 21/03/2022
 */
@ExtendWith(MockitoExtension.class)
class DocumentsMapperTest {
    
    @InjectMocks
    private DocumentsMapper classUnderTest = new DocumentsMapperImpl();
    
    @Mock
    private ContentMapper contentMapper;
    
    @Mock
    private DateConverter dateConverter;
    
    @Mock
    private CompanyUtils companyUtils;

    private Document       document;
    private DocumentBean   documentBean;
    private String         stringContent;
    private Timestamp      timestampDate;
    private OffsetDateTime offsetDateTime;
    private List<Section>  content;
    
    @BeforeEach
    void init() {
        Article article = new Article();
        article.setDesignation("article designation");
        article.setQuantity(2);
        article.setUnitPrice(16.90);
        
        Section section = new Section();
        section.setName("Section Name");
        section.setContent(Collections.singletonList(article));
        
        content = Collections.singletonList(section);
    
        offsetDateTime = OffsetDateTime.parse("2011-12-03T10:15:30+01:00");
        
        String clientName = "Client Name";
        String clientAddress = "123 CLoverField Street, 06410 Biot";
        
        String companyName = "Company Name";
        String companyAddress = "456 Place de la Concorde, 75000 Paris";
        String companyDescription = "Company Description";
        String companyLogo = "";
        String companyEmail = "company.contact@email.com";
        String companyTelephone = "0605040302";
        String companyBankDetails = "Bank Information";
        String companyInformation = "Company Information";
        
        String reference = "reference";
        String author = "D4RT4GNaN";
        Type type = Type.INVOICE;
        Status status = Status.PENDING;
        String location = "Paris";
        String subject = "Subject";
        String conditions = "Conditions";
        String paymentMethods = "Payment methods";
        String validityMessage = "Validity message";
        double totalExcludingTax = 16.90;
        double vat = 0.2;
        double totalWithTax = 20.38;
        
        document = new Document();
        document.setReference(reference);
        document.setAuthor(author);
        document.setClientName(clientName);
        document.setClientAddress(clientAddress);
        document.setCompanyName(companyName);
        document.setCompanyAddress(companyAddress);
        document.setCompanyDescription(companyDescription);
        document.setCompanyLogoSVG(companyLogo);
        document.setCompanyEmail(companyEmail);
        document.setCompanyTelephone(companyTelephone);
        document.setCompanyBankDetails(companyBankDetails);
        document.setCompanyInformation(companyInformation);
        document.setCreationDate(offsetDateTime);
        document.setCreationLocation(location);
        document.setUpdateDate(offsetDateTime);
        document.setSubjet(subject);
        document.setContent(content);
        document.setTotalAmountExcludingTax(totalExcludingTax);
        document.setTotalVAT(vat);
        document.setTotalAmountIncludingTax(totalWithTax);
        document.setConditions(conditions);
        document.setPaymentMethod(paymentMethods);
        document.setValidityMessage(validityMessage);
        document.setType(type);
        document.setStatus(status);
        
        document.setClientName(clientName);
        document.setClientAddress(clientAddress);
        
        document.setCompanyLogoSVG(companyLogo);
        document.setCompanyName(companyName);
        document.setCompanyAddress(companyAddress);
        document.setCompanyDescription(companyDescription);
        document.setCompanyEmail(companyEmail);
        document.setCompanyTelephone(companyTelephone);
        document.setCompanyBankDetails(companyBankDetails);
        document.setCompanyInformation(companyInformation);
        
        stringContent = "";
        
        timestampDate = Timestamp.valueOf("2011-12-03 10:15:30");
        
        documentBean = new DocumentBean();
        documentBean.setReference(reference);
        documentBean.setAuthor(author);
        documentBean.setType(type);
        documentBean.setStatus(status);
        documentBean.setCreationDate(timestampDate);
        documentBean.setCreationLocation(location);
        documentBean.setUpdateDate(timestampDate);
        documentBean.setSubject(subject);
        documentBean.setContent(stringContent);
        documentBean.setClientName(clientName);
        documentBean.setClientAddress(clientAddress);
        documentBean.setCompanyLogo(companyLogo);
        documentBean.setCompanyName(companyName);
        documentBean.setCompanyAddress(companyAddress);
        documentBean.setCompanyDescription(companyDescription);
        documentBean.setCompanyEmail(companyEmail);
        documentBean.setCompanyTelephone(companyTelephone);
        documentBean.setCompanyBankDetails(companyBankDetails);
        documentBean.setCompanyInformation(companyInformation);
        documentBean.setConditions(conditions);
        documentBean.setPaymentMethods(paymentMethods);
        documentBean.setValidity(validityMessage);
        documentBean.setTotalAmountExcludingTax(totalExcludingTax);
        documentBean.setTotalVAT(vat);
        documentBean.setTotalAmountIncludingTax(totalWithTax);
    }
    
    @Test
    void givenADocument_whenToDocumentBeanIsCalled_thenItReturnACorrespondingDocumentBean () {
        // GIVEN
        DocumentBean expected = documentBean;
        
        when(contentMapper.toString(any())).thenReturn(stringContent);
        when(dateConverter.toTimestamp(any())).thenReturn(timestampDate);
        
        // WHEN
        DocumentBean result = classUnderTest.toDocumentBean(document);
        
        // THEN
        assertThat(result).usingRecursiveComparison().isEqualTo(expected);
    }
    
    @Test
    void givenADocumentBean_whenToDocumentIsCalled_thenItReturnACorrespondingDocument () {
        // GIVEN
        Document expected = document;
        
        when(contentMapper.toListSection(any())).thenReturn(content);
        when(dateConverter.toOffsetDateTime(any())).thenReturn(offsetDateTime);
        
        // WHEN
        Document result = classUnderTest.toDocument(documentBean);
        
        // THEN
        assertThat(result).usingRecursiveComparison().isEqualTo(expected);
    }
    
    @Test
    void givenAPagedDocumentBeans_whenToDocumentsIsCalled_thenItReturnACorrespondingDocumentsList () {
        // GIVEN
        String reference2 = "reference 2";
        
        DocumentBean documentBean2 = documentBean;
        documentBean2.setReference(reference2);
    
        Page<DocumentBean> pagedDocumentBeans = new PageImpl<>(Arrays.asList(documentBean, documentBean2));
        
        Document document2 = document;
        document2.setReference(reference2);
        
        List<Document> expected = Arrays.asList(document, document2);
        
        when(contentMapper.toListSection(any())).thenReturn(content);
        when(dateConverter.toOffsetDateTime(any())).thenReturn(offsetDateTime);
        
        // WHEN
        List<Document> result = classUnderTest.toDocuments(pagedDocumentBeans);
        
        // THEN
        assertThat(result).usingRecursiveComparison().isEqualTo(expected);
    }
    
    @Test
    void givenADocument_whenUpdateDocumentBeanIsCalled_thenItReturnACorrespondingDocumentBean () {
        // GIVEN
        String newSubject = "new subject";
        document.setSubjet(newSubject);
        DocumentBean expected = documentBean;
        expected.setSubject(newSubject);
        
        when(contentMapper.toString(any())).thenReturn(stringContent);
        when(dateConverter.toTimestamp(any())).thenReturn(timestampDate);
        
        // WHEN
        DocumentBean result = classUnderTest.updateDocumentBean(document, documentBean);
        
        // THEN
        assertThat(result).usingRecursiveComparison().isEqualTo(expected);
    }
    
    @Test
    void givenValidParameters_whenSetNewDocument_thenItReturnANewPreFilledDocument () {
        // GIVEN
        String reference = document.getReference();
        Type type = Type.INVOICE;
        Company company = new Company();
        company.setLogoSVG(document.getCompanyLogoSVG());
        company.setFirstName("Ano");
        company.setLastName("Nymous");
        company.setAddress(document.getCompanyAddress());
        company.setBankDetail(document.getCompanyBankDetails());
        company.setEmail(document.getCompanyEmail());
        company.setDescription(document.getCompanyDescription());
        company.setName(document.getCompanyName());
        company.setTelephone(document.getCompanyTelephone());
        company.setOtherInformation(document.getCompanyInformation());
        
        DocumentInformation documentInformation = new DocumentInformation();
        documentInformation.setValidityMessage(document.getValidityMessage());
        documentInformation.setConditions(document.getConditions());
        documentInformation.setPaymentMethod(document.getPaymentMethod());
        
        Document expected = new Document();
        expected.setReference(reference);
        expected.setCompanyLogoSVG(document.getCompanyLogoSVG());
        expected.setAuthor("Ano Nymous");
        expected.setCompanyAddress(document.getCompanyAddress());
        expected.setCompanyBankDetails(document.getCompanyBankDetails());
        expected.setCompanyEmail(document.getCompanyEmail());
        expected.setCompanyDescription(document.getCompanyDescription());
        expected.setCompanyName(document.getCompanyName());
        expected.setCompanyTelephone(document.getCompanyTelephone());
        expected.setCompanyInformation(document.getCompanyInformation());
        expected.setCreationLocation(document.getCreationLocation());
        expected.setConditions(document.getConditions());
        expected.setStatus(Status.PENDING);
        expected.setPaymentMethod(document.getPaymentMethod());
        expected.setValidityMessage(document.getValidityMessage());
        expected.setType(type);
        
        when(companyUtils.getCity(anyString())).thenReturn("Paris");
        when(companyUtils.getAuthorFullName(any())).thenReturn("Ano Nymous");
        
        // WHEN
        Document result = classUnderTest.setNewDocument(reference, company, documentInformation, type);
        
        // THEN
        assertThat(result).usingRecursiveComparison().ignoringFields("creationDate", "updateDate").isEqualTo(expected);
    }
    
    @Test
    void givenANullCompany_whenSetNewDocument_thenItReturnANewPreFilledDocumentWithoutCompanyInformation () {
        // GIVEN
        String reference = document.getReference();
        Type type = Type.INVOICE;
        
        DocumentInformation documentInformation = new DocumentInformation();
        documentInformation.setValidityMessage(document.getValidityMessage());
        documentInformation.setConditions(document.getConditions());
        documentInformation.setPaymentMethod(document.getPaymentMethod());
        
        Document expected = new Document();
        expected.setReference(reference);
        expected.setConditions(document.getConditions());
        expected.setStatus(Status.PENDING);
        expected.setPaymentMethod(document.getPaymentMethod());
        expected.setValidityMessage(document.getValidityMessage());
        expected.setType(type);
        
        // WHEN
        Document result = classUnderTest.setNewDocument(reference, null, documentInformation, type);
        
        // THEN
        assertThat(result).usingRecursiveComparison().ignoringFields("creationDate", "updateDate").isEqualTo(expected);
    }
    
    @Test
    void givenANullDocumentInformation_whenSetNewDocument_thenItReturnANewPreFilledDocumentWithoutDocumentInformation () {
        // GIVEN
        String reference = document.getReference();
        Type type = Type.INVOICE;
        Company company = new Company();
        company.setLogoSVG(document.getCompanyLogoSVG());
        company.setFirstName("Ano");
        company.setLastName("Nymous");
        company.setAddress(document.getCompanyAddress());
        company.setBankDetail(document.getCompanyBankDetails());
        company.setEmail(document.getCompanyEmail());
        company.setDescription(document.getCompanyDescription());
        company.setName(document.getCompanyName());
        company.setTelephone(document.getCompanyTelephone());
        company.setOtherInformation(document.getCompanyInformation());
        
        Document expected = new Document();
        expected.setReference(reference);
        expected.setCompanyLogoSVG(document.getCompanyLogoSVG());
        expected.setAuthor("Ano Nymous");
        expected.setCompanyAddress(document.getCompanyAddress());
        expected.setCompanyBankDetails(document.getCompanyBankDetails());
        expected.setCompanyEmail(document.getCompanyEmail());
        expected.setCompanyDescription(document.getCompanyDescription());
        expected.setCompanyName(document.getCompanyName());
        expected.setCompanyTelephone(document.getCompanyTelephone());
        expected.setCompanyInformation(document.getCompanyInformation());
        expected.setCreationLocation(document.getCreationLocation());
        expected.setStatus(Status.PENDING);
        expected.setType(type);
        
        when(companyUtils.getCity(anyString())).thenReturn("Paris");
        when(companyUtils.getAuthorFullName(any())).thenReturn("Ano Nymous");
        
        // WHEN
        Document result = classUnderTest.setNewDocument(reference, company, null, type);
        
        // THEN
        assertThat(result).usingRecursiveComparison().ignoringFields("creationDate", "updateDate").isEqualTo(expected);
    }
    
}
