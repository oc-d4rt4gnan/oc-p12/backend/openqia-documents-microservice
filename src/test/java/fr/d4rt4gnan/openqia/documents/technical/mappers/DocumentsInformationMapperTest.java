package fr.d4rt4gnan.openqia.documents.technical.mappers;


import fr.d4rt4gnan.openqia.documents.model.DocumentInformation;
import fr.d4rt4gnan.openqia.documents.model.DocumentInformationBean;
import fr.d4rt4gnan.openqia.documents.technical.mappers.impl.DocumentsInformationMapperImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;


/**
 * @author D4RT4GNaN
 * @since 21/03/2022
 */
@ExtendWith(MockitoExtension.class)
class DocumentsInformationMapperTest {
    
    private DocumentsInformationMapper classUnderTest = new DocumentsInformationMapperImpl();
    
    private DocumentInformation     documentInformation;
    private DocumentInformationBean documentInformationBean;
    
    @BeforeEach
    void init () {
        UUID companyId = UUID.randomUUID();
        String conditions = "Conditions";
        String paymentMethods = "Payment methods";
        String validityMessage = "Validity message";
        
        documentInformation = new DocumentInformation();
        documentInformation.setCompanyId(companyId);
        documentInformation.setConditions(conditions);
        documentInformation.setPaymentMethod(paymentMethods);
        documentInformation.setValidityMessage(validityMessage);
        
        documentInformationBean = new DocumentInformationBean();
        documentInformationBean.setCompanyId(companyId);
        documentInformationBean.setConditions(conditions);
        documentInformationBean.setPaymentMethods(paymentMethods);
        documentInformationBean.setValidity(validityMessage);
    }
    
    @Test
    void givenADocumentInformation_whenToDocumentInformationBeanIsCalled_thenItReturnACorrespondingDocumentInformationBean () {
        // GIVEN
        DocumentInformationBean expected = documentInformationBean;
        
        // WHEN
        DocumentInformationBean result = classUnderTest.toDocumentInformationBean(documentInformation);
        
        // THEN
        assertThat(result).usingRecursiveComparison().isEqualTo(expected);
    }
    
    @Test
    void givenADocumentInformationBean_whenToDocumentInformationIsCalled_thenItReturnACorrespondingDocumentInformation () {
        // GIVEN
        DocumentInformation expected = documentInformation;
        
        // WHEN
        DocumentInformation result = classUnderTest.toDocumentInformation(documentInformationBean);
        
        // THEN
        assertThat(result).usingRecursiveComparison().isEqualTo(expected);
    }
    
    @Test
    void givenADocumentInformationAndADocumentInformationBean_whenUpdateIsCalled_thenItReturnACorrespondingDocumentInformationBean () {
        // GIVEN
        String validityMessage = "another validity message";
        documentInformation.setValidityMessage(validityMessage);
        DocumentInformationBean expected = documentInformationBean;
        expected.setValidity(validityMessage);
        
        // WHEN
        DocumentInformationBean result = classUnderTest.update(documentInformationBean, documentInformation);
        
        // THEN
        assertThat(result).usingRecursiveComparison().isEqualTo(expected);
    }
    
}
