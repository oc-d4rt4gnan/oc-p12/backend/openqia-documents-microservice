package fr.d4rt4gnan.openqia.documents.business;


import fr.d4rt4gnan.openqia.documents.api.DocumentsApiDelegate;
import fr.d4rt4gnan.openqia.documents.dao.DocumentsInformationRepository;
import fr.d4rt4gnan.openqia.documents.dao.DocumentsRepository;
import fr.d4rt4gnan.openqia.documents.model.*;
import fr.d4rt4gnan.openqia.documents.technical.mappers.DocumentsInformationMapper;
import fr.d4rt4gnan.openqia.documents.technical.mappers.DocumentsMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;


/**
 * @author D4RT4GNaN
 * @since 14/03/2022
 */
@ExtendWith(MockitoExtension.class)
class DocumentsServiceTest {
    
    @InjectMocks
    private DocumentsApiDelegate classUnderTest = new DocumentsService();
    
    @Mock
    private DocumentsMapper documentsMapper;
    
    @Mock
    private DocumentsRepository documentsRepository;
    
    @Mock
    private DocumentsInformationMapper documentsInformationMapper;
    
    @Mock
    private DocumentsInformationRepository documentsInformationRepository;
    
    @Test
    void givenAValidDocument_whenAddDocumentIsCalled_thenItReturnAResponse201 () {
        // GIVEN
        Document document = new Document();
        DocumentBean documentBean = new DocumentBean();
        
        when(documentsMapper.toDocumentBean(any())).thenReturn(documentBean);
        when(documentsRepository.save(any())).thenReturn(documentBean);
        when(documentsMapper.toDocument(any())).thenReturn(document);
        
        // WHEN
        ResponseEntity<Document> result = classUnderTest.addDocument(document);
        
        // THEN
        assertThat(result.getStatusCode()).isEqualByComparingTo(HttpStatus.CREATED);
    }
    
    @Test
    void givenAnInvalidDocument_whenAddDocumentIsCalled_thenItReturnAResponse400 () {
        // GIVEN -- Nothing to do
        
        // WHEN
        ResponseEntity<Document> result = classUnderTest.addDocument(null);
        
        // THEN
        assertThat(result.getStatusCode()).isEqualByComparingTo(HttpStatus.BAD_REQUEST);
    }
    
    @Test
    void givenAnExistingReference_whenGetDocumentIsCalled_thenItReturnAResponse200 () {
        // GIVEN
        String reference = "reference";
        Document document = new Document();
        DocumentBean documentBean = new DocumentBean();
    
        when(documentsRepository.getDocumentBeanByReferenceAndType(anyString(), any())).thenReturn(Optional.of(documentBean));
        when(documentsMapper.toDocument(any())).thenReturn(document);
        
        // WHEN
        ResponseEntity<Document> result = classUnderTest.getDocument(reference, Type.INVOICE);
        
        // THEN
        assertThat(result.getStatusCode()).isEqualByComparingTo(HttpStatus.OK);
    }
    
    @Test
    void givenAnInvalidReference_whenGetDocumentIsCalled_thenItReturnAResponse404 () {
        // GIVEN
        String reference = "reference";
    
        when(documentsRepository.getDocumentBeanByReferenceAndType(anyString(), any())).thenReturn(Optional.empty());
        
        // WHEN
        ResponseEntity<Document> result = classUnderTest.getDocument(reference, Type.INVOICE);
        
        // THEN
        assertThat(result.getStatusCode()).isEqualByComparingTo(HttpStatus.NOT_FOUND);
    }
    
    @Test
    void givenANullReference_whenGetDocumentIsCalled_thenItReturnAResponse400 () {
        // GIVEN -- Nothing to do
        
        // WHEN
        ResponseEntity<Document> result = classUnderTest.getDocument(null, null);
        
        // THEN
        assertThat(result.getStatusCode()).isEqualByComparingTo(HttpStatus.BAD_REQUEST);
    }
    
    @Test
    void givenAnExistingReferenceAndNotNullDocument_whenUpdateDocumentIsCalled_thenItReturnAResponse200 () {
        // GIVEN
        String reference = "reference";
        Document document = new Document();
        DocumentBean documentBean = new DocumentBean();
        
        when(documentsRepository.getDocumentBeanByReferenceAndType(anyString(), any())).thenReturn(Optional.of(documentBean));
        when(documentsMapper.updateDocumentBean(any(), any())).thenReturn(documentBean);
        when(documentsRepository.save(any())).thenReturn(documentBean);
        when(documentsMapper.toDocument(any())).thenReturn(document);
        
        // WHEN
        ResponseEntity<Document> result = classUnderTest.updateDocument(reference, document);
        
        // THEN
        assertThat(result.getStatusCode()).isEqualByComparingTo(HttpStatus.OK);
    }
    
    @Test
    void givenAnInvalidReference_whenUpdateDocumentIsCalled_thenItReturnAResponse404 () {
        // GIVEN
        String reference = "reference";
        Document document = new Document();
        
        when(documentsRepository.getDocumentBeanByReferenceAndType(anyString(), any())).thenReturn(Optional.empty());
        
        // WHEN
        ResponseEntity<Document> result = classUnderTest.updateDocument(reference, document);
        
        // THEN
        assertThat(result.getStatusCode()).isEqualByComparingTo(HttpStatus.NOT_FOUND);
    }
    
    @Test
    void givenANullReference_whenUpdateDocumentIsCalled_thenItReturnAResponse400 () {
        // GIVEN
        Document document = new Document();
        
        // WHEN
        ResponseEntity<Document> result = classUnderTest.updateDocument(null, document);
        
        // THEN
        assertThat(result.getStatusCode()).isEqualByComparingTo(HttpStatus.BAD_REQUEST);
    }
    
    @Test
    void givenANullDocument_whenUpdateDocumentIsCalled_thenItReturnAResponse400 () {
        // GIVEN
        String reference = "reference";
        
        // WHEN
        ResponseEntity<Document> result = classUnderTest.updateDocument(reference, null);
        
        // THEN
        assertThat(result.getStatusCode()).isEqualByComparingTo(HttpStatus.BAD_REQUEST);
    }
    
    @Test
    void givenAnExistingReference_whenDeleteDocumentIsCalled_thenItReturnAResponse200 () {
        // GIVEN
        String reference = "reference";
        DocumentBean documentBean = new DocumentBean();
        ResponseEntity<String> expected = new ResponseEntity<>(reference, HttpStatus.OK);
        
        when(documentsRepository.getDocumentBeanByReferenceAndType(anyString(), any())).thenReturn(Optional.of(documentBean));
        doNothing().when(documentsRepository).delete(any());
        
        // WHEN
        ResponseEntity<String> result = classUnderTest.deleteDocument(reference, Type.INVOICE);
        
        // THEN
        assertThat(result).usingRecursiveComparison().isEqualTo(expected);
    }
    
    @Test
    void givenAnInvalidReference_whenDeleteDocumentIsCalled_thenItReturnAResponse404 () {
        // GIVEN
        String reference = "reference";
        
        when(documentsRepository.getDocumentBeanByReferenceAndType(anyString(), any())).thenReturn(Optional.empty());
        
        // WHEN
        ResponseEntity<String> result = classUnderTest.deleteDocument(reference, Type.INVOICE);
        
        // THEN
        assertThat(result.getStatusCode()).isEqualByComparingTo(HttpStatus.NOT_FOUND);
    }
    
    @Test
    void givenANullReference_whenDeleteDocumentIsCalled_thenItReturnAResponse400 () {
        // GIVEN -- Nothing to do
        
        // WHEN
        ResponseEntity<String> result = classUnderTest.deleteDocument(null, null);
        
        // THEN
        assertThat(result.getStatusCode()).isEqualByComparingTo(HttpStatus.BAD_REQUEST);
    }
    
    @Test
    void givenAnInvoiceTypeAndPage0_whenGetDocumentsIsCalled_thenItReturnAResponse200WithTheListOfDocuments () {
        // GIVEN
        Document document = new Document();
        DocumentBean documentBean = new DocumentBean();
        
        Page<DocumentBean> pagedDocumentBeans = new PageImpl<>(Collections.singletonList(documentBean));
        List<Document> documents = Collections.singletonList(document);
        
        ResponseEntity<List<Document>> expected = ResponseEntity.ok().header("X-Total-Page","1").body(documents);
        
        when(documentsRepository.findDocumentBeansByTypeAndYear(any(), any(), any())).thenReturn(pagedDocumentBeans);
        when(documentsMapper.toDocuments(any())).thenReturn(documents);
        
        // WHEN
        ResponseEntity<List<Document>> result = classUnderTest.getDocuments(Type.INVOICE, 1, 2022, "");
        
        // THEN
        assertThat(result).usingRecursiveComparison().isEqualTo(expected);
    }
    
    @Test
    void givenAnInvoiceTypeAndPage0WithNoElement_whenGetDocumentsIsCalled_thenItReturnAResponse200WithAnEmptyListOfDocuments () {
        // GIVEN
        Page<DocumentBean> pagedDocumentBeans = new PageImpl<>(Collections.emptyList());
        List<Document> documents = Collections.emptyList();
    
        ResponseEntity<List<Document>> expected = ResponseEntity.ok().header("X-Total-Page","1").body(documents);
    
        when(documentsRepository.findDocumentBeansByTypeAndYear(any(), any(), any())).thenReturn(pagedDocumentBeans);
    
        // WHEN
        ResponseEntity<List<Document>> result = classUnderTest.getDocuments(Type.INVOICE, 1, 2022, "");
    
        // THEN
        assertThat(result).usingRecursiveComparison().isEqualTo(expected);
    }
    
    @Test
    void givenAnUndefinedType_whenGetDocumentsIsCalled_thenItReturnAResponse400 () {
        // GIVEN -- Nothing to do
        
        // WHEN
        ResponseEntity<List<Document>> result = classUnderTest.getDocuments(Type.UNDEFINED, 0, 2022, "");
        
        // THEN
        assertThat(result.getStatusCode()).isEqualByComparingTo(HttpStatus.BAD_REQUEST);
    }
    
    @Test
    void givenUnknownUUID_whenGetDocumentInformationIsCalled_thenItReturnANotFoundResponse () {
        // GIVEN
        when(documentsInformationRepository.findDocumentInformationBeanByCompanyId(any())).thenReturn(Optional.empty());
        
        // WHEN
        ResponseEntity<DocumentInformation> result = classUnderTest.getDocumentInformation(UUID.randomUUID());
        
        // THEN
        assertThat(result.getStatusCode()).isEqualByComparingTo(HttpStatus.NOT_FOUND);
    }
    
    @Test
    void givenKnownUUID_whenGetDocumentInformationIsCalled_thenItReturnTheCorrespondingDocumentInformation () {
        // GIVEN
        UUID companyId = UUID.randomUUID();
        
        DocumentInformationBean documentInformationBean = new DocumentInformationBean();
        documentInformationBean.setCompanyId(companyId);
        
        DocumentInformation documentInformation = new DocumentInformation();
        documentInformation.setCompanyId(companyId);
        
        ResponseEntity<DocumentInformation> expected = new ResponseEntity<>(documentInformation, HttpStatus.OK);
        
        when(documentsInformationRepository.findDocumentInformationBeanByCompanyId(any())).thenReturn(Optional.of(
                documentInformationBean));
        when(documentsInformationMapper.toDocumentInformation(any())).thenReturn(documentInformation);
    
        // WHEN
        ResponseEntity<DocumentInformation> result = classUnderTest.getDocumentInformation(companyId);
    
        // THEN
        assertThat(result).usingRecursiveComparison().isEqualTo(expected);
    }
    
    @Test
    void givenUUIDAlreadyExist_whenSaveDocumentInformationIsCalled_thenItReturnTheDocumentInformationBeanUpdated () {
        // GIVEN
        UUID companyId = UUID.randomUUID();
        String conditions = "conditions";
        String paymentMethod = "payment method";
    
        DocumentInformationBean documentInformationBean = new DocumentInformationBean();
        documentInformationBean.setCompanyId(companyId);
        documentInformationBean.setConditions(conditions);
        documentInformationBean.setPaymentMethods(paymentMethod);
        documentInformationBean.setValidity("validity");
    
        DocumentInformation documentInformation = new DocumentInformation();
        documentInformation.setCompanyId(companyId);
        documentInformation.setConditions(conditions);
        documentInformation.setPaymentMethod(paymentMethod);
        documentInformation.setValidityMessage("another validity message");
    
        ResponseEntity<DocumentInformation> expected = new ResponseEntity<>(documentInformation, HttpStatus.OK);
    
        when(documentsInformationRepository.findDocumentInformationBeanByCompanyId(any())).thenReturn(Optional.of(
                documentInformationBean));
        when(documentsInformationMapper.update(any(), any())).thenReturn(documentInformationBean);
        when(documentsInformationRepository.save(any())).thenReturn(documentInformationBean);
        when(documentsInformationMapper.toDocumentInformation(any())).thenReturn(documentInformation);
    
        // WHEN
        ResponseEntity<DocumentInformation> result = classUnderTest.saveDocumentInformation(companyId, documentInformation);
    
        // THEN
        assertThat(result).usingRecursiveComparison().isEqualTo(expected);
    }
    
    @Test
    void givenUnknownUUID_whenSaveDocumentInformationIsCalled_thenItReturnTheNewDocumentInformationBeanSaved () {
        // GIVEN
        UUID companyId = UUID.randomUUID();
        String conditions = "conditions";
        String paymentMethod = "payment method";
        String validity = "validity";
        
        DocumentInformationBean documentInformationBean = new DocumentInformationBean();
        documentInformationBean.setCompanyId(companyId);
        documentInformationBean.setConditions(conditions);
        documentInformationBean.setPaymentMethods(paymentMethod);
        documentInformationBean.setValidity(validity);
        
        DocumentInformation documentInformation = new DocumentInformation();
        documentInformation.setCompanyId(companyId);
        documentInformation.setConditions(conditions);
        documentInformation.setPaymentMethod(paymentMethod);
        documentInformation.setValidityMessage(validity);
        
        ResponseEntity<DocumentInformation> expected = new ResponseEntity<>(documentInformation, HttpStatus.OK);
        
        when(documentsInformationRepository.findDocumentInformationBeanByCompanyId(any())).thenReturn(Optional.empty());
        when(documentsInformationMapper.toDocumentInformationBean(any())).thenReturn(documentInformationBean);
        when(documentsInformationRepository.save(any())).thenReturn(documentInformationBean);
        when(documentsInformationMapper.toDocumentInformation(any())).thenReturn(documentInformation);
        
        // WHEN
        ResponseEntity<DocumentInformation> result = classUnderTest.saveDocumentInformation(companyId, documentInformation);
        
        // THEN
        assertThat(result).usingRecursiveComparison().isEqualTo(expected);
    }
    
}
